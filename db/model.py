# coding: utf-8
from sqlalchemy import Column, Date, ForeignKey, String
from sqlalchemy.dialects.mysql import INTEGER, TINYINT
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class PESPESSOA(Base):
    __tablename__ = 'PES_PESSOA'

    pes_id_pessoa = Column(INTEGER(11), primary_key=True, unique=True)
    pes_cpf_pessoa = Column(String(11), unique=True)
    pes_nm_pessoa = Column(String(50))


class TIUTIPOUSUARIO(Base):
    __tablename__ = 'TIU_TIPO_USUARIO'

    tiu_id_tipo_usuario = Column(INTEGER(11), primary_key=True, unique=True)
    tiu_ch_administrador = Column(TINYINT(1), nullable=False)
    tiu_ch_secretario = Column(TINYINT(1), nullable=False)


class VECVEICULO(Base):
    __tablename__ = 'VEC_VEICULO'

    vec_id_veiculo = Column(INTEGER(11), primary_key=True, unique=True)
    vec_ds_cor = Column(String(15))
    vec_ds_modelo = Column(String(15))
    vec_nu_placa = Column(String(8))


class AUTAUTORIZACAO(Base):
    __tablename__ = 'AUT_AUTORIZACAO'

    aut_id_autorizacao = Column(INTEGER(11), primary_key=True, unique=True)
    aut_id_pessoa = Column(ForeignKey('PES_PESSOA.pes_id_pessoa'), nullable=False, index=True)
    aut_id_veiculo = Column(ForeignKey('VEC_VEICULO.vec_id_veiculo'), index=True)
    aut_ds_autorizacao = Column(String(255))
    aut_dt_cadastro = Column(Date, nullable=False)
    aut_dt_expiracao = Column(Date, nullable=False)

    PES_PESSOA = relationship('PESPESSOA')
    VEC_VEICULO = relationship('VECVEICULO')


class USRUSUARIO(Base):
    __tablename__ = 'USR_USUARIO'

    usr_id_usuario = Column(INTEGER(11), primary_key=True, unique=True)
    usr_id_pessoa = Column(ForeignKey('PES_PESSOA.pes_id_pessoa'), nullable=False, unique=True)
    usr_id_tipo_usuario = Column(ForeignKey('TIU_TIPO_USUARIO.tiu_id_tipo_usuario'), nullable=False, index=True)
    usr_ds_senha = Column(String(255))
    usr_ds_nickname = Column(String(15))

    PES_PESSOA = relationship('PESPESSOA')
    TIU_TIPO_USUARIO = relationship('TIUTIPOUSUARIO')
