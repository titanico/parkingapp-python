from imports.UsuarioDAO import UsuarioDAO
from db.model import USRUSUARIO as User
from imports.login import login
from imports.auth import authentication

usrDAO = UsuarioDAO()
auth = authentication()

# Deletando usuário do banco
usrDAO.delete(usr_id_pessoa=11)

# Inserindo novo usuário no banco de dados
usuario = User()
usuario.usr_id_pessoa = 11
usuario.usr_id_tipo_usuario = 1
usuario.usr_ds_senha = auth.genpass("titan")
usuario.usr_ds_nickname = "ADMIN"
usrDAO.insert(usuario)


usr = input("Usuário: ")
pwd = input("Senha: ")

login = login(usr, pwd)

if (login.validate()):
    print ("Login Válido!")
else:
    print ("Usuário ou senha incorretos!")