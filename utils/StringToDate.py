import datetime as date

def StringToDate(string_data):
    # dd/mm/aaaa
    dia = int(string_data[0:2])
    mes = int(string_data[3:5])
    ano = int(string_data[6:])

    return date.datetime(ano, mes, dia)
