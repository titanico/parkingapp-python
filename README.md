# ParkingApp

Aplicativo para controle de estacionamento da Faculdade Politécnica da UFBA

# Instruções

As instruções a seguir são referentes às configurações e comandos básicos para desenvolvimento, teste e deploy da aplicação ParkingApp.

## Software necessário

    VSCode
    Anaconda
    MySQLServer
    MySQLClient
    Docker
    Git
    GitKraken
    Python (Versão >= 3.7.4)

## Virtual Enviroment

É **obrigatório** que após ter instalado o Anaconda, via terminal, digite:

    conda create --name ParkingApp_Dev

Para a criação de um ambiente virtual python dedicado apenas ao projeto.

Utilize o seguinte comando para começar a usar o ambiente virtual:

    conda activate ParkingApp_Dev

O ambiente virá sem pacotes instalados, instale-os segundo o tópico **Instalações**.

## Instalações

Ubuntu (em outras distribuições e sistemas, os pacotes equivalentes deverão ser instalados)

    sudo apt install libmysqlclient-dev
    sudo apt-get install build-essential
    sudo apt-get install libffi-dev


Gerais

    conda install pip
    pip install PyQt5
    pip install pyinstaller
    pip install pypiwin32
    pip install bcrypt
    pip install ConnectorDB
    pip install sqlalchemy
    pip install sqlacodegen
    pip install mysqlclient


Caso haja ausência de pacotes após as instalações citadas, reporte-as ao gerente do projeto e acrescente-as neste tópico.

## Inicialização do projeto
### Geração do banco de dados da aplicação

Com o Docker instalado e habilitado, acesse a pasta 'db' pelo terminal e execute:

    docker build -t mysql-parking .

E após:

    docker run -it -d -p 3307:3306 --name mysql mysql-parking

### Geração de hash da senha do usuário 'admin'

Ao inicializar o projeto pela primeira vez, via terminal de comando na pasta raíz do mesmo, execute:

    python init.py

## Execução do projeto

Para executar o projeto via terminal de comando, na pasta raíz do mesmo, execute:

    python main.py

No VSCode execute através do menu:

    Debug > Start Debug

## Outras informações

### Branch de liberação ao Atendimento

    master

### Branch de produção(desenvolvimento)

    release

Crie _branches_ a partir da _release_ para quaisquer alterações de código. Siga o seguinte padrão:

    hotfix_[nome da alteração]
    fix_[nome da alteração]
    feature_[nome da alteração]

### Comando para gerar executável (.exe):

#### Geração de Bundle(OneDir):

    [Windows] pyinstaller --distpath C:\Users\[Seu usuário]\Deploys\ParkingApp\release_vX.Y.Z --icon img\icones\icone_titan48x48.ico --version-file config\version_info.txt --onedir --windowed --win-private-assemblies main.py

#### Geração de executável único(OneFile):

    [Windows] pyinstaller --distpath C:\Users\[Seu usuário]\Deploys\ParkingApp\release_vX.Y.Z --icon img\icones\icone_titan48x48.ico --version-file config\version_info.txt --onefile --windowed --win-private-assemblies main.py

### WIKI

Para um maior detalhamento dos procedimentos para geração das classes Model, DAO, rotina de autenticação e integração com o banco de dados via Docker clique [aqui](https://bitbucket.org/titanico/parkingapp-python/wiki/Home).

### Links úteis:

[Windows] [Configurando terminal de python no VSCode](http://mscodingblog.blogspot.com/2017/08/setup-integrated-terminal-in-vs-code-to.html):

    C:\Windows\System32\cmd.exe

[Windows/Linux] [Gerenciando ambiente virtuais com o Anaconda](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands)
