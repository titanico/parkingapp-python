from imports.PessoaDAO import PessoaDAO
from db.model import PESPESSOA as Pessoa

pesDAO = PessoaDAO()

# # Deletando pessoa do banco(Só pode ser feito em pessoa sem autorizacao vinculada)
# pesDAO.deletePessoa(pes_id_pessoa=52)

# Inserindo nova pessoa no banco de dados
pessoa = Pessoa()
pessoa.pes_nm_pessoa = "Douglas Wiliam"
pessoa.pes_cpf_pessoa = "06289200593"
# pessoa.pes_id_pessoa = 24

pesDAO.insert(pessoa)

# Lendo pessoa do banco de dados 
pessoa2 = pesDAO.getPessoa(pes_id_pessoa=5)
print("Pessoa: \n", "Nome: ", pessoa2.pes_nm_pessoa, "\nCPF ", pessoa2.pes_cpf_pessoa)

# Atualizando dados de uma pessoa
pessoa2 = pesDAO.updatePessoa(pessoa2, "pes_cpf_pessoa", "41122330510")
pessoa2 = pesDAO.updatePessoa(pessoa2, "pes_nm_pessoa", "Sandra Cristina")
print("Pessoa: \n", "Nome: ", pessoa2.pes_nm_pessoa, "\nCPF: ", pessoa2.pes_cpf_pessoa)

# Listando todas as pessoas
print("Lista de pessoas na Tabela:")
lista = pesDAO.getQuery()
for p in lista:
    print(p.pes_id_pessoa, p.pes_nm_pessoa, p.pes_cpf_pessoa)
