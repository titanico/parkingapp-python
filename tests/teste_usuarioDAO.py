from imports.UsuarioDAO import UsuarioDAO
from db.model import USRUSUARIO as User
from imports.auth import authentication

usrDAO = UsuarioDAO()
auth = authentication()

# Deletando usuário do banco
usrDAO.delete(usr_id_pessoa=11)

# Inserindo novo usuário no banco de dados
usuario = User()
usuario.usr_id_pessoa = 11
usuario.usr_id_tipo_usuario = 1
usuario.usr_ds_senha = auth.genpass("titan")
usuario.usr_ds_nickname = "guiedington"
usrDAO.insert(usuario)

# Lendo usuario do banco de dados 
usuario2 = usrDAO.getUsuario(usr_ds_nickname='guiedington')
print("Usuario:", usuario2.usr_ds_nickname, "ID:",usuario2.usr_id_usuario)

# Atualizando dados de um usuário
usuario2.usr_ds_nickname = "teste"
usuario2 = usrDAO.update(usuario2,"usr_ds_nickname","teste")
print("Usuario:", usuario2.usr_ds_nickname, "ID:",usuario.usr_id_usuario)

# Listando todos os usuários
print("Lista de usuários da Tabela:")
lista = usrDAO.getQuery()
for u in lista:
    print(u.usr_id_usuario,u.usr_ds_nickname)

