from inc.auth import authentication

# Instânciando a classe authentication, nesse momento
# o Pepper é lido do arquivo ParkingApp.conf
auth = authentication()

password = input("Gerar Senha: ")
print("Encriptando password...")

# O método genpass irá retornar um hash da senha inserida
phash = auth.genpass(password)
print("OK! Hash: (",phash,")")
password = input("Testar Senha: ")

# O método check irá receber o hash da senha anterior e a senha atual
# irá testar se batem e retornará 0 ou -1
result = auth.check(password,phash)
if result == 0:
    print("OK!")
else:
    print("INCORRETO!")