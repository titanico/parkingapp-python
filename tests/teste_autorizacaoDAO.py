from imports.autorizacaoDAO import autorizacaoDAO
from db.model import AUTAUTORIZACAO as Autorizacao

authDAO = autorizacaoDAO()

# Deletando autorizacao do banco
authDAO.deleteAutorizacao(aut_id_autorizacao=10)

# Inserindo nova autorizacao no banco de dados
autorizacao = Autorizacao()
autorizacao.aut_id_veiculo = 2
autorizacao.aut_id_pessoa = 44
autorizacao.aut_dt_expiracao = "2019-03-23"
autorizacao.aut_dt_cadastro = "2019-06-01"
autorizacao.aut_ds_autorizacao = "Autorização de Dogão pq ele é otaku e foda e pegador"
authDAO.insert(autorizacao)

# Lendo autorizacao do banco de dados 
autorizacao2 = authDAO.getAutorizacao(aut_id_autorizacao=50)
print("Autorização: \n", "Data de cadastro: ", autorizacao2.aut_dt_cadastro, "\nData de expiração: ", autorizacao2.aut_dt_expiracao, "\nDescrição: ", autorizacao2.aut_ds_autorizacao)

# Atualizando dados de uma autorizacao
autorizacao2 = authDAO.updateAutorizacao(autorizacao2, "aut_ds_autorizacao", "Autorização de Douglas pq ele é otaku e foda")
autorizacao2 = authDAO.updateAutorizacao(autorizacao2, "aut_dt_cadastro", "2019-05-15")
autorizacao2 = authDAO.updateAutorizacao(autorizacao2, "aut_dt_expiracao", "2019-03-22")
print("Autorização: \n", "Data de cadastro: ", autorizacao2.aut_dt_cadastro, "\nData de expiração: ", autorizacao2.aut_dt_expiracao, "\nDescrição: ", autorizacao2.aut_ds_autorizacao)

# Inserindo nova autorizacao no banco de dados
autorizacao = Autorizacao()
autorizacao.aut_id_veiculo = 1
autorizacao.aut_id_pessoa = 45
autorizacao.aut_dt_expiracao = "2019-06-12"
autorizacao.aut_dt_cadastro = "2019-06-12"
autorizacao.aut_ds_autorizacao = "Miintirosos, sou romântico :3"
authDAO.insert(autorizacao)

# Listando todas autorizacoes
print("Lista de autorizações da Tabela:")
lista = authDAO.getQuery()
for a in lista:
    print(a.aut_id_autorizacao,a.aut_ds_autorizacao, a.aut_dt_cadastro, a.aut_dt_expiracao)