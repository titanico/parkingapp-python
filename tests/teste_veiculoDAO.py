from imports.veiculoDAO import veiculoDAO
from db.model import VECVEICULO as Veiculo

vecDAO = veiculoDAO()

# Deletando veiculo do banco
vecDAO.deleteVeiculo(vec_id_veiculo=4)

# Inserindo nova veiculo no banco de dados
veiculo = Veiculo()
veiculo.vec_ds_cor = "Vermelha"
veiculo.vec_ds_modelo = "Renault Clio"
veiculo.vec_id_pessoa = 24
veiculo.vec_nm_placa = "JJJT4444"

vecDAO.insert(veiculo)

# Lendo veiculo do banco de dados 
veiculo2 = vecDAO.getVeiculo(vec_id_veiculo=5)
print("Veiculo: \n", "Cor: ", veiculo2.vec_ds_cor, "\nModelo: ", veiculo2.vec_ds_modelo, "\nPlaca: ", veiculo2.vec_nm_placa)

# Atualizando dados de uma veiculo
veiculo2 = vecDAO.updateVeiculo(veiculo2, "vec_ds_cor", "Azul")
veiculo2 = vecDAO.updateVeiculo(veiculo2, "vec_ds_modelo", "Amarok")
veiculo2 = vecDAO.updateVeiculo(veiculo2, "vec_nm_placa", "JPJJ4567")
print("Veiculo: \n", "Cor: ", veiculo2.vec_ds_cor, "\nModelo: ", veiculo2.vec_ds_modelo, "\nPlaca: ", veiculo2.vec_nm_placa)

# Listando todas veiculo
print("Lista de veículos da Tabela:")
lista = vecDAO.getQuery()
for v in lista:
    print(v.vec_id_veiculo, v.vec_ds_cor, v.vec_ds_modelo, v.vec_nm_placa)