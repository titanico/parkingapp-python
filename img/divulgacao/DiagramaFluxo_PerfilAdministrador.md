```mermaid

graph TD

A(Login) --> B{Perfil}
B --> C1{Administrador}
C1--> D1{Pessoas}
C1--> E1{Veículos}
C1--> F1{Usuários}
C1 --> G1{Autorizações}
C1 --> H1{Configurações}

D1{Pessoas} --> I1[Busca]
D1 --> I2[Cadastro]
E1{Veículos} --> L1[Busca]
E1 --> L2[Cadastro]
F1{Usuários} --> M1[Busca]
F1 --> M2[Cadastro]
G1{Autorizações} --> P1[Busca]
G1 --> P2[Cadastro]
H1{Configurações}

I1 --> I1A{Busca por}
I1A --> I1A1[Nome]
I1A --> I1A2[CPF]

L1 --> L1A{Busca por}
L1A --> L1A1[Placa]
L1A --> L1A2[Cor]
L1A --> L1A3[Modelo]

M1 --> M1A{Busca por}
M1A --> M1A1[Usuário]
M1A --> M1A2[Tipo de usuário]

P1 --> P1A{Busca por}
P1A --> P1A1[Nome da pessoa]
P1A --> P1A2[CPF da pessoa]
P1A --> P1A3[Placa do veículo]

```