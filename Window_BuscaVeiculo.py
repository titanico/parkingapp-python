from PyQt5 import QtCore, QtGui, QtWidgets
from imports.VeiculoDAO import VeiculoDAO
from db.model import VECVEICULO as Veiculo

class Ui_Dialog(QtWidgets.QDialog):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(482, 368)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 6, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 5, 1, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 2, 0, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 6, 0, 1, 1)
        self.BuscaVeiculo_Table_Veiculos = QtWidgets.QTableWidget(Dialog)
        self.BuscaVeiculo_Table_Veiculos.setAutoScroll(True)
        self.BuscaVeiculo_Table_Veiculos.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.BuscaVeiculo_Table_Veiculos.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.BuscaVeiculo_Table_Veiculos.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.BuscaVeiculo_Table_Veiculos.setObjectName("BuscaVeiculo_Table_Veiculos")
        self.BuscaVeiculo_Table_Veiculos.setColumnCount(4)
        self.BuscaVeiculo_Table_Veiculos.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculo_Table_Veiculos.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculo_Table_Veiculos.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculo_Table_Veiculos.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculo_Table_Veiculos.setHorizontalHeaderItem(3, item)
        self.gridLayout.addWidget(self.BuscaVeiculo_Table_Veiculos, 3, 0, 2, 3)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem4, 5, 0, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem5, 5, 2, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem6, 2, 2, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem7, 2, 1, 1, 1)
        self.BuscaVeiculo_ButtonBox_Confirmacao = QtWidgets.QDialogButtonBox(Dialog)
        self.BuscaVeiculo_ButtonBox_Confirmacao.setOrientation(QtCore.Qt.Horizontal)
        self.BuscaVeiculo_ButtonBox_Confirmacao.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.BuscaVeiculo_ButtonBox_Confirmacao.setObjectName("BuscaVeiculo_ButtonBox_Confirmacao")
        self.gridLayout.addWidget(self.BuscaVeiculo_ButtonBox_Confirmacao, 6, 1, 1, 1)
        self.BuscaVeiculo_Button_Pesquisar = QtWidgets.QPushButton(Dialog)
        self.BuscaVeiculo_Button_Pesquisar.setObjectName("BuscaVeiculo_Button_Pesquisar")
        self.gridLayout.addWidget(self.BuscaVeiculo_Button_Pesquisar, 1, 2, 1, 1)
        self.BuscaVeiculo_ComboBox_filtro = QtWidgets.QComboBox(Dialog)
        self.BuscaVeiculo_ComboBox_filtro.setObjectName("BuscaVeiculo_ComboBox_filtro")
        self.BuscaVeiculo_ComboBox_filtro.addItem("")
        self.BuscaVeiculo_ComboBox_filtro.addItem("")
        self.BuscaVeiculo_ComboBox_filtro.addItem("")
        self.gridLayout.addWidget(self.BuscaVeiculo_ComboBox_filtro, 0, 2, 1, 1)
        self.BuscaVeiculo_Line_textoDeBusca = QtWidgets.QLineEdit(Dialog)
        self.BuscaVeiculo_Line_textoDeBusca.setObjectName("BuscaVeiculo_Line_textoDeBusca")
        self.gridLayout.addWidget(self.BuscaVeiculo_Line_textoDeBusca, 0, 0, 1, 2)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Busca de Veículos"))
        item = self.BuscaVeiculo_Table_Veiculos.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Id"))
        item = self.BuscaVeiculo_Table_Veiculos.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Nº da Placa"))
        item = self.BuscaVeiculo_Table_Veiculos.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "Modelo"))
        item = self.BuscaVeiculo_Table_Veiculos.horizontalHeaderItem(3)
        item.setText(_translate("Dialog", "Cor"))
        self.BuscaVeiculo_Button_Pesquisar.setText(_translate("Dialog", "Pesquisar"))
        self.BuscaVeiculo_ComboBox_filtro.setItemText(0, _translate("Dialog", "Nº da Placa"))
        self.BuscaVeiculo_ComboBox_filtro.setItemText(1, _translate("Dialog", "Modelo"))
        self.BuscaVeiculo_ComboBox_filtro.setItemText(2, _translate("Dialog", "Cor"))
        self.BuscaVeiculo_Line_textoDeBusca.setPlaceholderText(_translate("Dialog", "Digite o nº da placa do veículo"))

        #Configurações personalizadas
        self.initCustomConfig()

    def initCustomConfig(self):
        self.BuscaVeiculo_Table_Veiculos.setColumnHidden(0, True) #Ocultando coluna de ID
        self.setSignalsSlots()

    def setSignalsSlots(self):
        self.BuscaVeiculo_Button_Pesquisar.clicked.connect(lambda: self.populaTabela())
        self.BuscaVeiculo_ButtonBox_Confirmacao.buttons()[0].clicked.connect(lambda: self.getVeiculoSelecionado())
        self.BuscaVeiculo_ButtonBox_Confirmacao.buttons()[1].clicked.connect(lambda: self.reject())
        self.BuscaVeiculo_ComboBox_filtro.currentIndexChanged.connect(lambda: self.trocaPlaceHolderText())

    def populaTabela(self):
        self.BuscaVeiculo_Table_Veiculos.setRowCount(0)
        veiculos = self.buscaVeiculo()

        print(type(veiculos))
        for v in veiculos:
            posicaoLinha = self.BuscaVeiculo_Table_Veiculos.rowCount()
            self.BuscaVeiculo_Table_Veiculos.insertRow(posicaoLinha)
            self.BuscaVeiculo_Table_Veiculos.setItem(posicaoLinha, 0 , QtWidgets.QTableWidgetItem(str(v.vec_id_veiculo)))
            self.BuscaVeiculo_Table_Veiculos.setItem(posicaoLinha, 1 , QtWidgets.QTableWidgetItem(v.vec_nu_placa))
            self.BuscaVeiculo_Table_Veiculos.setItem(posicaoLinha, 2, QtWidgets.QTableWidgetItem(v.vec_ds_modelo))
            self.BuscaVeiculo_Table_Veiculos.setItem(posicaoLinha, 3, QtWidgets.QTableWidgetItem(v.vec_ds_cor))

    def buscaVeiculo(self):
        veiculoDAO = VeiculoDAO()
        textoDeBusca = self.BuscaVeiculo_Line_textoDeBusca.text()

        if(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 0):
            query = veiculoDAO.getQuery(vec_nu_placa=textoDeBusca)

        elif(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 1):
            query = veiculoDAO.getQuery(vec_ds_modelo='%'+textoDeBusca+'%')

        elif(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 2):
            query = veiculoDAO.getQuery(vec_ds_cor='%'+textoDeBusca+'%')
        
        return query

    def trocaPlaceHolderText(self):
        if(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 0):
            self.BuscaVeiculo_Line_textoDeBusca.setPlaceholderText("Digite o nº da placa do veículo")
            
        elif(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 1):
            self.BuscaVeiculo_Line_textoDeBusca.setPlaceholderText("Digite o modelo do veículo")
              
        elif(self.BuscaVeiculo_ComboBox_filtro.currentIndex() == 2):
            self.BuscaVeiculo_Line_textoDeBusca.setPlaceholderText("Digite a cor do veículo")

    def getVeiculoSelecionado(self):
        if(self.BuscaVeiculo_Table_Veiculos.rowCount() > 0):
            if(len(self.BuscaVeiculo_Table_Veiculos.selectedItems())):
                placa = self.BuscaVeiculo_Table_Veiculos.selectedItems()[0]
                modelo = self.BuscaVeiculo_Table_Veiculos.selectedItems()[1]
                cor = self.BuscaVeiculo_Table_Veiculos.selectedItems()[2]

                veiculo = Veiculo()
                veiculo.vec_id_veiculo = int(self.BuscaVeiculo_Table_Veiculos.item(placa.row(),0).text(), base=10)

                veiculo.vec_nu_placa = placa.text()
                veiculo.vec_ds_modelo = modelo.text()
                veiculo.vec_ds_cor = cor.text()

                self.accept()
                #DEBUG#
                # print("ID: " + str(veiculo.vec_id_veiculo))
                # print("Placa: " + veiculo.vec_nu_placa)
                # print("Modelo: " + veiculo.vec_ds_modelo)
                # print("Cor: " + veiculo.vec_ds_cor )
                # #DEBUG#
                return veiculo
            else:
                self.reject()
                #DEBUG#
                print("Retornando nada! Nenhum veículo selecionado.")
                #DEBUG#
                return None

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_Dialog()
    ui.setupUi(ui)
    ui.show()
    sys.exit(app.exec_())

