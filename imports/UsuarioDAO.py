from db.model import USRUSUARIO as Usuario
from imports.connector import connectorDB
from sqlalchemy import update

class UsuarioDAO:
    def __init__(self):
        self.dbconn = connectorDB().getSession()
        
    def insert(self, usuario : Usuario):
        self.dbconn.add(usuario)
        self.dbconn.commit()

    def getQuery(self, usr_id_usuario=None, usr_id_pessoa=None, usr_id_tipo_usuario=None,  usr_ds_senha=None, usr_ds_nickname=None):
        if (usr_ds_nickname is not None):
            query = self.dbconn.query(Usuario).filter(Usuario.usr_ds_nickname.like(usr_ds_nickname)).all()
        else:
            kwargs = {}
            if usr_id_usuario is not None: kwargs["usr_id_usuario"] = usr_id_usuario
            if usr_id_pessoa is not None: kwargs["usr_id_pessoa"] = usr_id_pessoa
            if usr_id_tipo_usuario is not None: kwargs["usr_id_tipo_usuario"] = usr_id_tipo_usuario
            if usr_ds_senha is not None: kwargs["usr_ds_senha"] = usr_ds_senha
            query = self.dbconn.query(Usuario).filter_by(**kwargs).all()

        return query

    def getUsuario( self, usr_id_usuario=None, usr_id_pessoa=None, usr_ds_nickname=None):
        query = self.getQuery(usr_id_usuario=usr_id_usuario, usr_id_pessoa=usr_id_pessoa, usr_ds_nickname=usr_ds_nickname)
        return query[0]
    
    def delete(self, usr_id_usuario=None, usr_id_pessoa=None, usr_ds_nickname=None):
        query = self.getQuery(usr_id_usuario=usr_id_usuario, usr_id_pessoa=usr_id_pessoa, usr_ds_nickname=usr_ds_nickname)
        query.delete()
        self.dbconn.commit()
    
    def update(self, usuario : Usuario, field, value):
        statement = update(Usuario).where(Usuario.usr_id_usuario == usuario.usr_id_usuario).values(usr_ds_senha=value)
        self.dbconn.execute(statement)
        self.dbconn.commit()
        return self.getUsuario(usr_id_usuario=usuario.usr_id_usuario)
