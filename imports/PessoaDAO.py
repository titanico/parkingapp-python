from db.model import PESPESSOA as Pessoa
from imports.connector import connectorDB


class PessoaDAO:

	def __init__(self):
		self.dbconn = connectorDB().getSession()

	def insert(self, pessoa: Pessoa):
		self.dbconn.add(pessoa)
		self.dbconn.commit()
	
	def getQuery(self, pes_id_pessoa=None, pes_cpf_pessoa=None, pes_nm_pessoa=None):
		if ((pes_nm_pessoa is not None) and (pes_cpf_pessoa is None) and (pes_id_pessoa is None)):
			query = self.dbconn.query(Pessoa).filter(Pessoa.pes_nm_pessoa.like(pes_nm_pessoa)).all()
		else:
			kwargs = {}
			if pes_id_pessoa is not None: kwargs["pes_id_pessoa"] = pes_id_pessoa
			if pes_cpf_pessoa is not None: kwargs["pes_cpf_pessoa"] = pes_cpf_pessoa
			query = self.dbconn.query(Pessoa).filter_by(**kwargs).all()

		return query
	
	def getPessoa(self, pes_id_pessoa=None, pes_cpf_pessoa=None):
		query = self.getQuery(pes_id_pessoa, pes_cpf_pessoa, None)
		return query[0] 
	
	def delete(self, pes_id_pessoa=None, pes_cpf_pessoa=None):
		query = self.getPessoa(pes_id_pessoa, pes_cpf_pessoa)
		query.delete()
		self.dbconn.commit()
	
	def update(self, pessoa : Pessoa, field, value):
		query = self.getPessoa(pes_id_pessoa=pessoa.pes_id_pessoa)
		query.update({field : value})
		self.dbconn.commit()
		return self.getPessoa(pes_id_pessoa=pessoa.pes_id_pessoa)
