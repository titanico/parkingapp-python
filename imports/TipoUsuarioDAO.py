from db.model import TIUTIPOUSUARIO as TipoUsuario
from imports.connector import connectorDB

    
class TipoUsuarioDAO:
	def __init__(self):
		self.dbconn = connectorDB().getSession()
	
	def insert(self,tipousuario: TipoUsuario):
		self.dbconn.add(tipousuario)
		self.dbconn.commit()

	def getQuery(self, tiu_id_tipo_usuario=None, tiu_ch_administrador=None, tiu_ch_secretario=None):
		kwargs = {}
		if tiu_id_tipo_usuario is not None: kwargs["tiu_id_tipo_usuario"] = tiu_id_tipo_usuario
		if tiu_ch_administrador is not None: kwargs["tiu_ch_administrador"] = tiu_ch_administrador
		if tiu_ch_secretario is not None: kwargs["tiu_ch_secretario"] = tiu_ch_secretario
		query = self.dbconn.query(TipoUsuario).filter_by(**kwargs)
		return query
	
	def getTipoUsuario(self, tiu_id_tipo_usuario=None, tiu_ch_administrador=None, tiu_ch_secretario=None):
		query = self.getQuery(tiu_id_tipo_usuario, tiu_ch_administrador, tiu_ch_secretario)
		return query[0]

	def deleteTipoUsuario(self, tiu_id_tipo_usuario=None, tiu_ch_administrador=None, tiu_ch_secretario=None):
		query = self.getQuery(tiu_id_tipo_usuario, tiu_ch_administrador, tiu_ch_secretario)
		query.delete()
		self.dbconn.commit()
	
	def updateTipoUsuario(self, tipousuario : TipoUsuario, field, value):
		query = self.getQuery(tiu_id_tipo_usuario=tipousuario.tiu_id_tipo_usuario)
		query.update({field : value})
		self.dbconn.commit()
		return self.getTipoUsuario(tiu_id_tipo_usuario=tipousuario.tiu_id_tipo_usuario)

