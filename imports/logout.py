import os, sys

#Função para reinicializar as variáveis e objetos do programa.
def logout():
    os.execl(sys.executable, sys.executable, *sys.argv)
    return
