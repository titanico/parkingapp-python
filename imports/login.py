from imports.auth import authentication
from imports.UsuarioDAO import UsuarioDAO

class login:
    def __init__(self, username, pwd):
        self.username = username
        self.pwd = pwd
        self.userDAO = UsuarioDAO()
        self.auth = authentication()
        
    def validate(self):
        try:
            self.user = self.userDAO.getUsuario(usr_ds_nickname=self.username)
        except IndexError:
            return False
        return self.auth.check(self.pwd,self.user.usr_ds_senha)
            
        

