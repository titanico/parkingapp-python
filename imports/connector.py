from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class connectorDB():
    def __init__(self):
        server = open("/etc/ParkingApp.conf","r")
        database_server_ip = str(server.readline().split("\n")[0])
        self.engine = create_engine('mysql://root:titan@'+ database_server_ip  + ':3307/parkingdb')
        self.engine.connect()
        self.Session = sessionmaker(bind=self.engine)
        self.session = self.Session()

    def getSession(self):
        return self.session
