import bcrypt

class authentication():
    def __init__(self):
        configFile = open("/etc/ParkingApp.conf","r")
        configFile.readline()
        self.pepper = configFile.readline().split("\n")[0].encode("utf-8")
        configFile.close()
    
    def encrypt(self,passwd,salt):
        hashed = bcrypt.hashpw(passwd.encode("utf-8")+self.pepper,salt)
        hashed = hashed.decode("utf-8")
        return hashed
    
    def genpass(self,passwd):
        salt = bcrypt.gensalt()
        return self.encrypt(passwd,salt)

    def check(self,passwd,phash):
        salt = phash[:29].encode("utf-8")
        hashed = self.encrypt(passwd,salt)
        if ( hashed == phash ):
            return True
        else:
            return False



