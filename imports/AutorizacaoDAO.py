from db.model import AUTAUTORIZACAO as Autorizacao
from imports.connector import connectorDB

class AutorizacaoDAO:

    def __init__(self):
        self.dbconn = connectorDB().getSession()

    def insert(self, autorizacao: Autorizacao):
        self.dbconn.add(autorizacao)
        self.dbconn.commit()
    
    def getQuery(self, aut_id_autorizacao=None, aut_id_pessoa=None, aut_id_veiculo=None, aut_ds_autorizacao=None, aut_dt_cadastro=None, aut_dt_expiracao=None):
        if((aut_id_autorizacao is None) and (aut_ds_autorizacao is not None)):
            query = self.dbconn.query(Autorizacao).filter(Autorizacao.aut_ds_autorizacao.like(aut_ds_autorizacao)).all()
        else:
            kwargs = {}
            if aut_id_autorizacao is not None: kwargs["aut_id_autorizacao"] = aut_id_autorizacao
            if aut_id_pessoa is not None: kwargs["aut_id_pessoa"] = aut_id_pessoa
            if aut_id_veiculo is not None: kwargs["aut_id_veiculo"] = aut_id_veiculo
            if aut_ds_autorizacao is not None: kwargs["aut_ds_autorizacao"] = aut_ds_autorizacao
            if aut_dt_cadastro is not None: kwargs["aut_dt_cadastro"] = aut_dt_cadastro
            if aut_dt_expiracao is not None: kwargs["aut_dt_expiracao"] = aut_dt_expiracao
            query = self.dbconn.query(Autorizacao).filter_by(**kwargs).all()
        return query
    
    def getAutorizacao(self, aut_id_autorizacao=None):
        query = self.getQuery(aut_id_autorizacao=aut_id_autorizacao)
        return query[0]

    def delete(self, aut_id_autorizacao=None):
        query = self.getQuery(aut_id_autorizacao=aut_id_autorizacao)
        query.delete()
        self.dbconn.commit()

    def update(self, autorizacao : Autorizacao, field, value):
        query = self.getQuery(aut_id_autorizacao=autorizacao.aut_id_autorizacao)
        query.update({field : value})
        self.dbconn.commit()
        return self.getAutorizacao(aut_id_autorizacao=autorizacao.aut_id_autorizacao)