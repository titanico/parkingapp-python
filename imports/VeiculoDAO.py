from db.model import VECVEICULO as Veiculo
from imports.connector import connectorDB

class VeiculoDAO:

    def __init__(self):
        self.dbconn = connectorDB().getSession()

    def insert(self, veiculo: Veiculo):
        self.dbconn.add(veiculo)
        self.dbconn.commit()
    
    def getQuery(self, vec_id_veiculo=None, vec_ds_cor=None, vec_ds_modelo=None, vec_nu_placa=None):
        if((vec_id_veiculo is None) and (vec_nu_placa is None) and (vec_ds_modelo is not None or vec_ds_cor is not None)):
            if(vec_ds_modelo is not None):
                query = self.dbconn.query(Veiculo).filter(Veiculo.vec_ds_modelo.like(vec_ds_modelo)).all()
            elif(vec_ds_cor is not None):
                query = self.dbconn.query(Veiculo).filter(Veiculo.vec_ds_cor.like(vec_ds_cor)).all()
        else:
            kwargs = {}
            if vec_id_veiculo is not None: kwargs["vec_id_veiculo"] = vec_id_veiculo
            if vec_nu_placa is not None: kwargs["vec_nu_placa"] = vec_nu_placa
            query = self.dbconn.query(Veiculo).filter_by(**kwargs).all()

        return query
        
    def getVeiculo(self, vec_id_veiculo=None, vec_nu_placa=None):
        query = self.getQuery(vec_id_veiculo, vec_nu_placa)
        return query[0]

    def delete(self, vec_id_veiculo=None, vec_nu_placa=None):
        query = self.getVeiculo(vec_id_veiculo, vec_nu_placa)
        query.delete()
        self.dbconn.commit()

    def update(self, veiculo : Veiculo, field, value):
        query = self.getVeiculo(vec_id_veiculo=veiculo.vec_id_veiculo)
        query.update({field : value})
        self.dbconn.commit()
        return self.getVeiculo(vec_id_veiculo=veiculo.vec_id_veiculo)