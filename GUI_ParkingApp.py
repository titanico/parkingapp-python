from db.model import PESPESSOA as Pessoa, VECVEICULO as Veiculo, AUTAUTORIZACAO as Autorizacao, USRUSUARIO as Usuario, TIUTIPOUSUARIO as TipoUsuario
from Window_BuscaVeiculo import Ui_Dialog as janelaVeiculo
from Window_BuscaPessoa import Ui_Dialog as janelaPessoa
from imports.AutorizacaoDAO import AutorizacaoDAO
from utils.StringToDate import StringToDate
from PyQt5 import QtCore, QtGui, QtWidgets
from imports.VeiculoDAO import VeiculoDAO
from imports.PessoaDAO import PessoaDAO
from imports.UsuarioDAO import UsuarioDAO
from imports.TipoUsuarioDAO import TipoUsuarioDAO
from imports.auth import authentication as Autenticacao
import datetime
import sys


def getNomeTela(index):
    #Indexes
    """
    0 - menu de pessoas
    1 - menu  de veículos
    2 - menu de usuários
    3 - menu de autorizações
    4 - busca de pessoas
    5 - busca de veiculos
    6 - busca de usuários
    7 - busca de autorizações
    8 - cadastro de pessoas
    9 - cadastro de veiculos
    10 - cadastro de usuarios
    11 - cadastro de autorizações
    """
    text = "ParkingApp"
    
    if(index == 0):
        text = "Menu de pessoas"
    if(index == 1):
        text = "Menu de veículos"
    if(index == 2):
        text = "Menu de usuários"
    if(index == 3):
        text = "Menu de autorizações"
    if(index == 4):
        text = "Busca de pessoas"
    if(index == 5):
        text = "Busca de veículos"
    if(index == 6):
        text = "Busca de usuários"
    if(index == 7):
        text = "Busca de autorizações"
    if(index == 8):
        text = "Cadastro de pessoas"
    if(index == 9):
        text = "Cadastro de veículos"
    if(index == 10):
        text = "Cadastro de usuários"
    if(index == 11):
        text = "Cadastro de autorizações"
    return text

class Ui_MainWindow(QtWidgets.QMainWindow):    
 
 
    ## CRIAÇÃO, SETUP E EVENTOS ##

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1016, 611)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout_23 = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout_23.setObjectName("horizontalLayout_23")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.Menu_Label_tituloParkingApp = QtWidgets.QLabel(self.centralwidget)
        self.Menu_Label_tituloParkingApp.setObjectName("Menu_Label_tituloParkingApp")
        self.horizontalLayout_2.addWidget(self.Menu_Label_tituloParkingApp)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Menu_Label_nomeUsuario = QtWidgets.QLabel(self.centralwidget)
        self.Menu_Label_nomeUsuario.setScaledContents(False)
        self.Menu_Label_nomeUsuario.setObjectName("Menu_Label_nomeUsuario")
        self.horizontalLayout.addWidget(self.Menu_Label_nomeUsuario)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.Menu_Button_pessoas = QtWidgets.QPushButton(self.centralwidget)
        self.Menu_Button_pessoas.setObjectName("Menu_Button_pessoas")
        self.verticalLayout.addWidget(self.Menu_Button_pessoas)
        self.Menu_Button_veiculos = QtWidgets.QPushButton(self.centralwidget)
        self.Menu_Button_veiculos.setObjectName("Menu_Button_veiculos")
        self.verticalLayout.addWidget(self.Menu_Button_veiculos)
        self.Menu_Button_usuarios = QtWidgets.QPushButton(self.centralwidget)
        self.Menu_Button_usuarios.setObjectName("Menu_Button_usuarios")
        self.verticalLayout.addWidget(self.Menu_Button_usuarios)
        self.Menu_Button_autorizacoes = QtWidgets.QPushButton(self.centralwidget)
        self.Menu_Button_autorizacoes.setObjectName("Menu_Button_autorizacoes")
        self.verticalLayout.addWidget(self.Menu_Button_autorizacoes)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.Menu_Button_logout = QtWidgets.QPushButton(self.centralwidget)
        self.Menu_Button_logout.setEnabled(True)
        self.Menu_Button_logout.setObjectName("Menu_Button_logout")
        self.verticalLayout.addWidget(self.Menu_Button_logout)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.horizontalLayout_3.addWidget(self.line_3)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem1)
        self.Geral_Label_tituloDaTela = QtWidgets.QLabel(self.centralwidget)
        self.Geral_Label_tituloDaTela.setObjectName("Geral_Label_tituloDaTela")
        self.horizontalLayout_7.addWidget(self.Geral_Label_tituloDaTela)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.verticalLayout_2.addLayout(self.horizontalLayout_7)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_3.addWidget(self.line_2)
        self.tab_telas = QtWidgets.QTabWidget(self.centralwidget)
        self.tab_telas.setObjectName("tab_telas")
        self.MenuPessoas_tab = QtWidgets.QWidget()
        self.MenuPessoas_tab.setObjectName("MenuPessoas_tab")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.MenuPessoas_tab)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.MenuPessoas_Button_cadastrar = QtWidgets.QPushButton(self.MenuPessoas_tab)
        self.MenuPessoas_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuPessoas_Button_cadastrar.setObjectName("MenuPessoas_Button_cadastrar")
        self.horizontalLayout_5.addWidget(self.MenuPessoas_Button_cadastrar)
        self.MenuPessoas_Button_buscar = QtWidgets.QPushButton(self.MenuPessoas_tab)
        self.MenuPessoas_Button_buscar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuPessoas_Button_buscar.setObjectName("MenuPessoas_Button_buscar")
        self.horizontalLayout_5.addWidget(self.MenuPessoas_Button_buscar)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem4)
        self.verticalLayout_4.addLayout(self.horizontalLayout_5)
        self.tab_telas.addTab(self.MenuPessoas_tab, "")
        self.MenuVeiculos_tab = QtWidgets.QWidget()
        self.MenuVeiculos_tab.setObjectName("MenuVeiculos_tab")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout(self.MenuVeiculos_tab)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.horizontalLayout_30 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_30.setObjectName("horizontalLayout_30")
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_30.addItem(spacerItem5)
        self.MenuVeiculos_Button_cadastrar = QtWidgets.QPushButton(self.MenuVeiculos_tab)
        self.MenuVeiculos_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuVeiculos_Button_cadastrar.setObjectName("MenuVeiculos_Button_cadastrar")
        self.horizontalLayout_30.addWidget(self.MenuVeiculos_Button_cadastrar)
        self.MenuVeiculos_Button_buscar = QtWidgets.QPushButton(self.MenuVeiculos_tab)
        self.MenuVeiculos_Button_buscar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuVeiculos_Button_buscar.setObjectName("MenuVeiculos_Button_buscar")
        self.horizontalLayout_30.addWidget(self.MenuVeiculos_Button_buscar)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_30.addItem(spacerItem6)
        self.verticalLayout_10.addLayout(self.horizontalLayout_30)
        self.tab_telas.addTab(self.MenuVeiculos_tab, "")
        self.MenuUser_tab = QtWidgets.QWidget()
        self.MenuUser_tab.setObjectName("MenuUser_tab")
        self.horizontalLayout_13 = QtWidgets.QHBoxLayout(self.MenuUser_tab)
        self.horizontalLayout_13.setObjectName("horizontalLayout_13")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem7)
        self.MenuUsuarios_Button_cadastrar = QtWidgets.QPushButton(self.MenuUser_tab)
        self.MenuUsuarios_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuUsuarios_Button_cadastrar.setAutoFillBackground(False)
        self.MenuUsuarios_Button_cadastrar.setCheckable(False)
        self.MenuUsuarios_Button_cadastrar.setObjectName("MenuUsuarios_Button_cadastrar")
        self.horizontalLayout_8.addWidget(self.MenuUsuarios_Button_cadastrar)
        self.MenuUsuarios_Button_buscar = QtWidgets.QPushButton(self.MenuUser_tab)
        self.MenuUsuarios_Button_buscar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuUsuarios_Button_buscar.setObjectName("MenuUsuarios_Button_buscar")
        self.horizontalLayout_8.addWidget(self.MenuUsuarios_Button_buscar)
        spacerItem8 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem8)
        self.horizontalLayout_13.addLayout(self.horizontalLayout_8)
        self.tab_telas.addTab(self.MenuUser_tab, "")
        self.MenuAutorizacoes_tab = QtWidgets.QWidget()
        self.MenuAutorizacoes_tab.setObjectName("MenuAutorizacoes_tab")
        self.horizontalLayout_33 = QtWidgets.QHBoxLayout(self.MenuAutorizacoes_tab)
        self.horizontalLayout_33.setObjectName("horizontalLayout_33")
        self.horizontalLayout_31 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_31.setObjectName("horizontalLayout_31")
        spacerItem9 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_31.addItem(spacerItem9)
        self.MenuAutorizacoes_Button_cadastrar = QtWidgets.QPushButton(self.MenuAutorizacoes_tab)
        self.MenuAutorizacoes_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuAutorizacoes_Button_cadastrar.setObjectName("MenuAutorizacoes_Button_cadastrar")
        self.horizontalLayout_31.addWidget(self.MenuAutorizacoes_Button_cadastrar)
        self.MenuAutorizacoes_Button_buscar = QtWidgets.QPushButton(self.MenuAutorizacoes_tab)
        self.MenuAutorizacoes_Button_buscar.setMinimumSize(QtCore.QSize(175, 55))
        self.MenuAutorizacoes_Button_buscar.setObjectName("MenuAutorizacoes_Button_buscar")
        self.horizontalLayout_31.addWidget(self.MenuAutorizacoes_Button_buscar)
        spacerItem10 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_31.addItem(spacerItem10)
        self.horizontalLayout_33.addLayout(self.horizontalLayout_31)
        self.tab_telas.addTab(self.MenuAutorizacoes_tab, "")
        self.BuscaPessoas_tab = QtWidgets.QWidget()
        self.BuscaPessoas_tab.setObjectName("BuscaPessoas_tab")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout(self.BuscaPessoas_tab)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.BuscaPessoas_Line_campo = QtWidgets.QLineEdit(self.BuscaPessoas_tab)
        self.BuscaPessoas_Line_campo.setText("")
        self.BuscaPessoas_Line_campo.setObjectName("BuscaPessoas_Line_campo")
        self.horizontalLayout_4.addWidget(self.BuscaPessoas_Line_campo)
        self.BuscaPessoas_Label_buscarPor = QtWidgets.QLabel(self.BuscaPessoas_tab)
        self.BuscaPessoas_Label_buscarPor.setObjectName("BuscaPessoas_Label_buscarPor")
        self.horizontalLayout_4.addWidget(self.BuscaPessoas_Label_buscarPor)
        self.BuscaPessoas_ComboBox_chaveBusca = QtWidgets.QComboBox(self.BuscaPessoas_tab)
        self.BuscaPessoas_ComboBox_chaveBusca.setObjectName("BuscaPessoas_ComboBox_chaveBusca")
        self.BuscaPessoas_ComboBox_chaveBusca.addItem("")
        self.BuscaPessoas_ComboBox_chaveBusca.addItem("")
        self.horizontalLayout_4.addWidget(self.BuscaPessoas_ComboBox_chaveBusca)
        self.BuscaPessoas_Button_buscar = QtWidgets.QPushButton(self.BuscaPessoas_tab)
        self.BuscaPessoas_Button_buscar.setObjectName("BuscaPessoas_Button_buscar")
        self.horizontalLayout_4.addWidget(self.BuscaPessoas_Button_buscar)
        self.gridLayout.addLayout(self.horizontalLayout_4, 0, 1, 1, 1)
        self.BuscaPessoas_Table_tabelaPessoas = QtWidgets.QTableWidget(self.BuscaPessoas_tab)
        self.BuscaPessoas_Table_tabelaPessoas.setObjectName("BuscaPessoas_Table_tabelaPessoas")
        self.BuscaPessoas_Table_tabelaPessoas.setColumnCount(2)
        self.BuscaPessoas_Table_tabelaPessoas.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaPessoas_Table_tabelaPessoas.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaPessoas_Table_tabelaPessoas.setHorizontalHeaderItem(1, item)
        self.gridLayout.addWidget(self.BuscaPessoas_Table_tabelaPessoas, 1, 1, 1, 1)
        self.horizontalLayout_6.addLayout(self.gridLayout)
        self.tab_telas.addTab(self.BuscaPessoas_tab, "")
        self.BuscaVeiculos_tab = QtWidgets.QWidget()
        self.BuscaVeiculos_tab.setObjectName("BuscaVeiculos_tab")
        self.horizontalLayout_35 = QtWidgets.QHBoxLayout(self.BuscaVeiculos_tab)
        self.horizontalLayout_35.setObjectName("horizontalLayout_35")
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.horizontalLayout_34 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_34.setObjectName("horizontalLayout_34")
        self.BuscaVeiculos_Line_campo = QtWidgets.QLineEdit(self.BuscaVeiculos_tab)
        self.BuscaVeiculos_Line_campo.setText("")
        self.BuscaVeiculos_Line_campo.setObjectName("BuscaVeiculos_Line_campo")
        self.horizontalLayout_34.addWidget(self.BuscaVeiculos_Line_campo)
        self.BuscaVeiculso_Label_buscarPor = QtWidgets.QLabel(self.BuscaVeiculos_tab)
        self.BuscaVeiculso_Label_buscarPor.setObjectName("BuscaVeiculso_Label_buscarPor")
        self.horizontalLayout_34.addWidget(self.BuscaVeiculso_Label_buscarPor)
        self.BuscaVeiculos_ComboBox_chaveBusca = QtWidgets.QComboBox(self.BuscaVeiculos_tab)
        self.BuscaVeiculos_ComboBox_chaveBusca.setObjectName("BuscaVeiculos_ComboBox_chaveBusca")
        self.BuscaVeiculos_ComboBox_chaveBusca.addItem("")
        self.BuscaVeiculos_ComboBox_chaveBusca.addItem("")
        self.BuscaVeiculos_ComboBox_chaveBusca.addItem("")
        self.horizontalLayout_34.addWidget(self.BuscaVeiculos_ComboBox_chaveBusca)
        self.BuscaVeiculos_Button_buscar = QtWidgets.QPushButton(self.BuscaVeiculos_tab)
        self.BuscaVeiculos_Button_buscar.setObjectName("BuscaVeiculos_Button_buscar")
        self.horizontalLayout_34.addWidget(self.BuscaVeiculos_Button_buscar)
        self.gridLayout_3.addLayout(self.horizontalLayout_34, 0, 1, 1, 1)
        self.BuscaVeiculos_Table_tabelaVeiculos = QtWidgets.QTableWidget(self.BuscaVeiculos_tab)
        self.BuscaVeiculos_Table_tabelaVeiculos.setObjectName("BuscaVeiculos_Table_tabelaVeiculos")
        self.BuscaVeiculos_Table_tabelaVeiculos.setColumnCount(3)
        self.BuscaVeiculos_Table_tabelaVeiculos.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculos_Table_tabelaVeiculos.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculos_Table_tabelaVeiculos.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaVeiculos_Table_tabelaVeiculos.setHorizontalHeaderItem(2, item)
        self.gridLayout_3.addWidget(self.BuscaVeiculos_Table_tabelaVeiculos, 1, 1, 1, 1)
        self.horizontalLayout_35.addLayout(self.gridLayout_3)
        self.tab_telas.addTab(self.BuscaVeiculos_tab, "")
        self.BuscUser_tab = QtWidgets.QWidget()
        self.BuscUser_tab.setObjectName("BuscUser_tab")
        self.horizontalLayout_15 = QtWidgets.QHBoxLayout(self.BuscUser_tab)
        self.horizontalLayout_15.setObjectName("horizontalLayout_15")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.horizontalLayout_14 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_14.setObjectName("horizontalLayout_14")
        self.BuscaUsuarios_Line_campo = QtWidgets.QLineEdit(self.BuscUser_tab)
        self.BuscaUsuarios_Line_campo.setText("")
        self.BuscaUsuarios_Line_campo.setObjectName("BuscaUsuarios_Line_campo")
        self.horizontalLayout_14.addWidget(self.BuscaUsuarios_Line_campo)
        self.BuscaUsuarios_Label_buscarPor = QtWidgets.QLabel(self.BuscUser_tab)
        self.BuscaUsuarios_Label_buscarPor.setObjectName("BuscaUsuarios_Label_buscarPor")
        self.horizontalLayout_14.addWidget(self.BuscaUsuarios_Label_buscarPor)
        self.BuscaUsuarios_ComboBox_ChaveBusca = QtWidgets.QComboBox(self.BuscUser_tab)
        self.BuscaUsuarios_ComboBox_ChaveBusca.setObjectName("BuscaUsuarios_ComboBox_ChaveBusca")
        self.BuscaUsuarios_ComboBox_ChaveBusca.addItem("")
        self.BuscaUsuarios_ComboBox_ChaveBusca.addItem("")
        self.BuscaUsuarios_ComboBox_ChaveBusca.addItem("")
        self.horizontalLayout_14.addWidget(self.BuscaUsuarios_ComboBox_ChaveBusca)
        self.BuscaUsuarios_Button_buscar = QtWidgets.QPushButton(self.BuscUser_tab)
        self.BuscaUsuarios_Button_buscar.setObjectName("BuscaUsuarios_Button_buscar")
        self.horizontalLayout_14.addWidget(self.BuscaUsuarios_Button_buscar)
        self.gridLayout_2.addLayout(self.horizontalLayout_14, 0, 1, 1, 1)
        self.BuscaUsuarios_Table_tabelaUsuarios = QtWidgets.QTableWidget(self.BuscUser_tab)
        self.BuscaUsuarios_Table_tabelaUsuarios.setObjectName("BuscaUsuarios_Table_tabelaUsuarios")
        self.BuscaUsuarios_Table_tabelaUsuarios.setColumnCount(3)
        self.BuscaUsuarios_Table_tabelaUsuarios.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaUsuarios_Table_tabelaUsuarios.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaUsuarios_Table_tabelaUsuarios.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaUsuarios_Table_tabelaUsuarios.setHorizontalHeaderItem(2, item)
        self.gridLayout_2.addWidget(self.BuscaUsuarios_Table_tabelaUsuarios, 1, 1, 1, 1)
        self.horizontalLayout_15.addLayout(self.gridLayout_2)
        self.tab_telas.addTab(self.BuscUser_tab, "")
        self.BuscaAutorizacoes_tab = QtWidgets.QWidget()
        self.BuscaAutorizacoes_tab.setObjectName("BuscaAutorizacoes_tab")
        self.horizontalLayout_37 = QtWidgets.QHBoxLayout(self.BuscaAutorizacoes_tab)
        self.horizontalLayout_37.setObjectName("horizontalLayout_37")
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.horizontalLayout_36 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_36.setObjectName("horizontalLayout_36")
        self.BuscaAutorizacoes_Line_campoBusca = QtWidgets.QLineEdit(self.BuscaAutorizacoes_tab)
        self.BuscaAutorizacoes_Line_campoBusca.setText("")
        self.BuscaAutorizacoes_Line_campoBusca.setObjectName("BuscaAutorizacoes_Line_campoBusca")
        self.horizontalLayout_36.addWidget(self.BuscaAutorizacoes_Line_campoBusca)
        self.BuscaAutorizacoes_Label_buscarPor = QtWidgets.QLabel(self.BuscaAutorizacoes_tab)
        self.BuscaAutorizacoes_Label_buscarPor.setObjectName("BuscaAutorizacoes_Label_buscarPor")
        self.horizontalLayout_36.addWidget(self.BuscaAutorizacoes_Label_buscarPor)
        self.BuscaAutorizacoes_ComboBox_chaveBusca = QtWidgets.QComboBox(self.BuscaAutorizacoes_tab)
        self.BuscaAutorizacoes_ComboBox_chaveBusca.setObjectName("BuscaAutorizacoes_ComboBox_chaveBusca")
        self.BuscaAutorizacoes_ComboBox_chaveBusca.addItem("")
        self.BuscaAutorizacoes_ComboBox_chaveBusca.addItem("")
        self.BuscaAutorizacoes_ComboBox_chaveBusca.addItem("")
        self.horizontalLayout_36.addWidget(self.BuscaAutorizacoes_ComboBox_chaveBusca)
        self.BuscaAutorizacoes_Button_buscar = QtWidgets.QPushButton(self.BuscaAutorizacoes_tab)
        self.BuscaAutorizacoes_Button_buscar.setObjectName("BuscaAutorizacoes_Button_buscar")
        self.horizontalLayout_36.addWidget(self.BuscaAutorizacoes_Button_buscar)
        self.gridLayout_4.addLayout(self.horizontalLayout_36, 0, 1, 1, 1)
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes = QtWidgets.QTableWidget(self.BuscaAutorizacoes_tab)
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setObjectName("BuscaAutorizacoes_Table_tabelaAutorizacoes")
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setColumnCount(5)
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setHorizontalHeaderItem(3, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setHorizontalHeaderItem(4, item)
        self.gridLayout_4.addWidget(self.BuscaAutorizacoes_Table_tabelaAutorizacoes, 1, 1, 1, 1)
        self.horizontalLayout_37.addLayout(self.gridLayout_4)
        self.tab_telas.addTab(self.BuscaAutorizacoes_tab, "")
        self.CadastroPessoa_tab = QtWidgets.QWidget()
        self.CadastroPessoa_tab.setObjectName("CadastroPessoa_tab")
        self.horizontalLayout_28 = QtWidgets.QHBoxLayout(self.CadastroPessoa_tab)
        self.horizontalLayout_28.setObjectName("horizontalLayout_28")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.horizontalLayout_24 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_24.setObjectName("horizontalLayout_24")
        self.verticalLayout_8.addLayout(self.horizontalLayout_24)
        self.formLayout_4 = QtWidgets.QFormLayout()
        self.formLayout_4.setObjectName("formLayout_4")
        self.CadastroPessoa_Label_nome = QtWidgets.QLabel(self.CadastroPessoa_tab)
        self.CadastroPessoa_Label_nome.setObjectName("CadastroPessoa_Label_nome")
        self.formLayout_4.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.CadastroPessoa_Label_nome)
        self.horizontalLayout_25 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_25.setObjectName("horizontalLayout_25")
        self.CadastroPessoa_Line_nome = QtWidgets.QLineEdit(self.CadastroPessoa_tab)
        self.CadastroPessoa_Line_nome.setText("")
        self.CadastroPessoa_Line_nome.setReadOnly(False)
        self.CadastroPessoa_Line_nome.setObjectName("CadastroPessoa_Line_nome")
        self.horizontalLayout_25.addWidget(self.CadastroPessoa_Line_nome)
        self.formLayout_4.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_25)
        self.CadastroPessoa_Label_CPF = QtWidgets.QLabel(self.CadastroPessoa_tab)
        self.CadastroPessoa_Label_CPF.setObjectName("CadastroPessoa_Label_CPF")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.CadastroPessoa_Label_CPF)
        self.CadastroPessoa_Line_CPF = QtWidgets.QLineEdit(self.CadastroPessoa_tab)
        self.CadastroPessoa_Line_CPF.setObjectName("CadastroPessoa_Line_CPF")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.CadastroPessoa_Line_CPF)
        self.horizontalLayout_26 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_26.setObjectName("horizontalLayout_26")
        self.formLayout_4.setLayout(4, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_26)
        spacerItem11 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_4.setItem(1, QtWidgets.QFormLayout.FieldRole, spacerItem11)
        self.verticalLayout_8.addLayout(self.formLayout_4)
        spacerItem12 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem12)
        self.horizontalLayout_27 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_27.setObjectName("horizontalLayout_27")
        spacerItem13 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_27.addItem(spacerItem13)
        self.CadastroPessoa_Button_cadastrar = QtWidgets.QPushButton(self.CadastroPessoa_tab)
        self.CadastroPessoa_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 45))
        self.CadastroPessoa_Button_cadastrar.setObjectName("CadastroPessoa_Button_cadastrar")
        self.horizontalLayout_27.addWidget(self.CadastroPessoa_Button_cadastrar)
        spacerItem14 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_27.addItem(spacerItem14)
        self.verticalLayout_8.addLayout(self.horizontalLayout_27)
        self.horizontalLayout_28.addLayout(self.verticalLayout_8)
        self.tab_telas.addTab(self.CadastroPessoa_tab, "")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.tab)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout()
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.horizontalLayout_29 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_29.setObjectName("horizontalLayout_29")
        self.verticalLayout_9.addLayout(self.horizontalLayout_29)
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.CadastroVeiculos_Label_placa = QtWidgets.QLabel(self.tab)
        self.CadastroVeiculos_Label_placa.setObjectName("CadastroVeiculos_Label_placa")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.CadastroVeiculos_Label_placa)
        self.CadastroVeiculos_Line_placa = QtWidgets.QLineEdit(self.tab)
        self.CadastroVeiculos_Line_placa.setObjectName("CadastroVeiculos_Line_placa")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.CadastroVeiculos_Line_placa)
        spacerItem15 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout.setItem(1, QtWidgets.QFormLayout.FieldRole, spacerItem15)
        self.CadastroVeiculos_Label_modelo = QtWidgets.QLabel(self.tab)
        self.CadastroVeiculos_Label_modelo.setObjectName("CadastroVeiculos_Label_modelo")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.CadastroVeiculos_Label_modelo)
        self.CadastroVeiculos_Line_modelo = QtWidgets.QLineEdit(self.tab)
        self.CadastroVeiculos_Line_modelo.setObjectName("CadastroVeiculos_Line_modelo")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.CadastroVeiculos_Line_modelo)
        spacerItem16 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout.setItem(3, QtWidgets.QFormLayout.FieldRole, spacerItem16)
        self.CadastroVeiculos_Line_cor = QtWidgets.QLineEdit(self.tab)
        self.CadastroVeiculos_Line_cor.setObjectName("CadastroVeiculos_Line_cor")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.CadastroVeiculos_Line_cor)
        self.CadastroVeiculos_Label_cor = QtWidgets.QLabel(self.tab)
        self.CadastroVeiculos_Label_cor.setObjectName("CadastroVeiculos_Label_cor")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.CadastroVeiculos_Label_cor)
        self.verticalLayout_9.addLayout(self.formLayout)
        spacerItem17 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_9.addItem(spacerItem17)
        self.horizontalLayout_32 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_32.setObjectName("horizontalLayout_32")
        spacerItem18 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_32.addItem(spacerItem18)
        self.CadastroVeiculo_Button_cadastrar = QtWidgets.QPushButton(self.tab)
        self.CadastroVeiculo_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 45))
        self.CadastroVeiculo_Button_cadastrar.setObjectName("CadastroVeiculo_Button_cadastrar")
        self.horizontalLayout_32.addWidget(self.CadastroVeiculo_Button_cadastrar)
        spacerItem19 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_32.addItem(spacerItem19)
        self.verticalLayout_9.addLayout(self.horizontalLayout_32)
        self.verticalLayout_5.addLayout(self.verticalLayout_9)
        self.tab_telas.addTab(self.tab, "")
        self.CadUser_tab = QtWidgets.QWidget()
        self.CadUser_tab.setObjectName("CadUser_tab")
        self.horizontalLayout_19 = QtWidgets.QHBoxLayout(self.CadUser_tab)
        self.horizontalLayout_19.setObjectName("horizontalLayout_19")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.horizontalLayout_16 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_16.setObjectName("horizontalLayout_16")
        self.verticalLayout_7.addLayout(self.horizontalLayout_16)
        self.formLayout_3 = QtWidgets.QFormLayout()
        self.formLayout_3.setObjectName("formLayout_3")
        self.CadastroUser_Label_pessoa = QtWidgets.QLabel(self.CadUser_tab)
        self.CadastroUser_Label_pessoa.setObjectName("CadastroUser_Label_pessoa")
        self.formLayout_3.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.CadastroUser_Label_pessoa)
        self.horizontalLayout_21 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_21.setObjectName("horizontalLayout_21")
        self.CadastroUser_Line_pessoa = QtWidgets.QLineEdit(self.CadUser_tab)
        self.CadastroUser_Line_pessoa.setReadOnly(True)
        self.CadastroUser_Line_pessoa.setObjectName("CadastroUser_Line_pessoa")
        self.horizontalLayout_21.addWidget(self.CadastroUser_Line_pessoa)
        self.CadastroUser_Button_pessoaBuscar = QtWidgets.QPushButton(self.CadUser_tab)
        self.CadastroUser_Button_pessoaBuscar.setObjectName("CadastroUser_Button_pessoaBuscar")
        self.horizontalLayout_21.addWidget(self.CadastroUser_Button_pessoaBuscar)
        self.formLayout_3.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_21)
        spacerItem20 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_3.setItem(1, QtWidgets.QFormLayout.FieldRole, spacerItem20)
        self.CadastroUser_Label_user = QtWidgets.QLabel(self.CadUser_tab)
        self.CadastroUser_Label_user.setObjectName("CadastroUser_Label_user")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.CadastroUser_Label_user)
        self.CadastroUser_Line_usuario = QtWidgets.QLineEdit(self.CadUser_tab)
        self.CadastroUser_Line_usuario.setObjectName("CadastroUser_Line_usuario")
        self.formLayout_3.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.CadastroUser_Line_usuario)
        self.CadastroUser_Label_senha = QtWidgets.QLabel(self.CadUser_tab)
        self.CadastroUser_Label_senha.setObjectName("CadastroUser_Label_senha")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.CadastroUser_Label_senha)
        self.CadastroUser_Line_senha = QtWidgets.QLineEdit(self.CadUser_tab)
        self.CadastroUser_Line_senha.setEchoMode(QtWidgets.QLineEdit.Password)
        self.CadastroUser_Line_senha.setObjectName("CadastroUser_Line_senha")
        self.formLayout_3.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.CadastroUser_Line_senha)
        self.CadastroUser_Label_confSenha = QtWidgets.QLabel(self.CadUser_tab)
        self.CadastroUser_Label_confSenha.setObjectName("CadastroUser_Label_confSenha")
        self.formLayout_3.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.CadastroUser_Label_confSenha)
        self.CadastroUser_Line_confirmarSenha = QtWidgets.QLineEdit(self.CadUser_tab)
        self.CadastroUser_Line_confirmarSenha.setEchoMode(QtWidgets.QLineEdit.Password)
        self.CadastroUser_Line_confirmarSenha.setObjectName("CadastroUser_Line_confirmarSenha")
        self.formLayout_3.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.CadastroUser_Line_confirmarSenha)
        spacerItem21 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_3.setItem(5, QtWidgets.QFormLayout.FieldRole, spacerItem21)
        self.CadastroUser_Label_tipoUser = QtWidgets.QLabel(self.CadUser_tab)
        self.CadastroUser_Label_tipoUser.setObjectName("CadastroUser_Label_tipoUser")
        self.formLayout_3.setWidget(6, QtWidgets.QFormLayout.LabelRole, self.CadastroUser_Label_tipoUser)
        self.horizontalLayout_17 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_17.setObjectName("horizontalLayout_17")
        self.CadastroUser_ComboBox_tipoUsser = QtWidgets.QComboBox(self.CadUser_tab)
        self.CadastroUser_ComboBox_tipoUsser.setMinimumSize(QtCore.QSize(0, 0))
        self.CadastroUser_ComboBox_tipoUsser.setObjectName("CadastroUser_ComboBox_tipoUsser")
        self.CadastroUser_ComboBox_tipoUsser.addItem("")
        self.CadastroUser_ComboBox_tipoUsser.addItem("")
        self.horizontalLayout_17.addWidget(self.CadastroUser_ComboBox_tipoUsser)
        spacerItem22 = QtWidgets.QSpacerItem(5, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_17.addItem(spacerItem22)
        self.formLayout_3.setLayout(6, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_17)
        self.verticalLayout_7.addLayout(self.formLayout_3)
        spacerItem23 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_7.addItem(spacerItem23)
        self.horizontalLayout_18 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_18.setObjectName("horizontalLayout_18")
        spacerItem24 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_18.addItem(spacerItem24)
        self.CadastroUser_Button_cadastrar = QtWidgets.QPushButton(self.CadUser_tab)
        self.CadastroUser_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 45))
        self.CadastroUser_Button_cadastrar.setObjectName("CadastroUser_Button_cadastrar")
        self.horizontalLayout_18.addWidget(self.CadastroUser_Button_cadastrar)
        spacerItem25 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_18.addItem(spacerItem25)
        self.verticalLayout_7.addLayout(self.horizontalLayout_18)
        self.horizontalLayout_19.addLayout(self.verticalLayout_7)
        self.tab_telas.addTab(self.CadUser_tab, "")
        self.CadastroAutorizacao_tab = QtWidgets.QWidget()
        self.CadastroAutorizacao_tab.setObjectName("CadastroAutorizacao_tab")
        self.horizontalLayout_11 = QtWidgets.QHBoxLayout(self.CadastroAutorizacao_tab)
        self.horizontalLayout_11.setObjectName("horizontalLayout_11")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.verticalLayout_6.addLayout(self.horizontalLayout_9)
        self.formLayout_2 = QtWidgets.QFormLayout()
        self.formLayout_2.setObjectName("formLayout_2")
        self.CadastroAutorizacao_Label_pessoa = QtWidgets.QLabel(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Label_pessoa.setObjectName("CadastroAutorizacao_Label_pessoa")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.CadastroAutorizacao_Label_pessoa)
        self.horizontalLayout_20 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_20.setObjectName("horizontalLayout_20")
        self.CadastroAuto_Line_pessoa = QtWidgets.QLineEdit(self.CadastroAutorizacao_tab)
        self.CadastroAuto_Line_pessoa.setObjectName("CadastroAuto_Line_pessoa")
        self.horizontalLayout_20.addWidget(self.CadastroAuto_Line_pessoa)
        self.CadastroAutorizacao_Button_pessoaBuscar = QtWidgets.QPushButton(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Button_pessoaBuscar.setObjectName("CadastroAutorizacao_Button_pessoaBuscar")
        self.horizontalLayout_20.addWidget(self.CadastroAutorizacao_Button_pessoaBuscar)
        self.formLayout_2.setLayout(0, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_20)
        self.CadastroAutorizacao_Label_veiculo = QtWidgets.QLabel(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Label_veiculo.setObjectName("CadastroAutorizacao_Label_veiculo")
        self.formLayout_2.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.CadastroAutorizacao_Label_veiculo)
        self.horizontalLayout_22 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_22.setObjectName("horizontalLayout_22")
        self.CadastroAuto_Line_veiculo = QtWidgets.QLineEdit(self.CadastroAutorizacao_tab)
        self.CadastroAuto_Line_veiculo.setObjectName("CadastroAuto_Line_veiculo")
        self.horizontalLayout_22.addWidget(self.CadastroAuto_Line_veiculo)
        self.CadastroAutorizacao_Button_veiculoBuscar = QtWidgets.QPushButton(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Button_veiculoBuscar.setObjectName("CadastroAutorizacao_Button_veiculoBuscar")
        self.horizontalLayout_22.addWidget(self.CadastroAutorizacao_Button_veiculoBuscar)
        self.formLayout_2.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_22)
        spacerItem26 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_2.setItem(2, QtWidgets.QFormLayout.FieldRole, spacerItem26)
        self.CadastroAutorizacao_Label_validade = QtWidgets.QLabel(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Label_validade.setObjectName("CadastroAutorizacao_Label_validade")
        self.formLayout_2.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.CadastroAutorizacao_Label_validade)
        self.horizontalLayout_12 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_12.setObjectName("horizontalLayout_12")
        self.CadastroAuto_Line_validade = QtWidgets.QLineEdit(self.CadastroAutorizacao_tab)
        self.CadastroAuto_Line_validade.setObjectName("CadastroAuto_Line_validade")
        self.horizontalLayout_12.addWidget(self.CadastroAuto_Line_validade)
        spacerItem27 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_12.addItem(spacerItem27)
        self.formLayout_2.setLayout(3, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_12)
        spacerItem28 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.formLayout_2.setItem(4, QtWidgets.QFormLayout.FieldRole, spacerItem28)
        self.CadastroAuto_Label_descricao = QtWidgets.QLabel(self.CadastroAutorizacao_tab)
        self.CadastroAuto_Label_descricao.setObjectName("CadastroAuto_Label_descricao")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.CadastroAuto_Label_descricao)
        self.CadastroAuto_TextBox_descricao = QtWidgets.QTextEdit(self.CadastroAutorizacao_tab)
        self.CadastroAuto_TextBox_descricao.setObjectName("CadastroAuto_TextBox_descricao")
        self.formLayout_2.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.CadastroAuto_TextBox_descricao)
        self.verticalLayout_6.addLayout(self.formLayout_2)
        spacerItem29 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem29)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        spacerItem30 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_10.addItem(spacerItem30)
        self.CadastroAutorizacao_Button_cadastrar = QtWidgets.QPushButton(self.CadastroAutorizacao_tab)
        self.CadastroAutorizacao_Button_cadastrar.setMinimumSize(QtCore.QSize(175, 45))
        self.CadastroAutorizacao_Button_cadastrar.setObjectName("CadastroAutorizacao_Button_cadastrar")
        self.horizontalLayout_10.addWidget(self.CadastroAutorizacao_Button_cadastrar)
        spacerItem31 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_10.addItem(spacerItem31)
        self.verticalLayout_6.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11.addLayout(self.verticalLayout_6)
        self.tab_telas.addTab(self.CadastroAutorizacao_tab, "")
        self.verticalLayout_3.addWidget(self.tab_telas)
        self.horizontalLayout_3.addLayout(self.verticalLayout_3)
        self.horizontalLayout_23.addLayout(self.horizontalLayout_3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1016, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tab_telas.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def initCustomConfig(self):
            self.tab_telas.tabBar().hide()
            self.tab_telas.setTabEnabled(0, False)
            self.tab_telas.setTabEnabled(1, False)
            self.tab_telas.setTabEnabled(2, False)
            self.tab_telas.setTabEnabled(3, False)
            self.tab_telas.setTabEnabled(4, False)
            self.tab_telas.setTabEnabled(5, False)
            self.tab_telas.setTabEnabled(6, False)
            self.tab_telas.setTabEnabled(7, False)
            self.tab_telas.setTabEnabled(8, False)
            self.tab_telas.setTabEnabled(9, False)
            self.tab_telas.setTabEnabled(10, False)
            self.tab_telas.setTabEnabled(11, False)
            self.tab_telas.setStyleSheet("QTabBar::tab::disabled {width: 0; height: 0; margin: 0; padding: 0; border: none;} ")
            self.setSignalsSlots()
            self.pessoa_objeto = None
            self.veiculo_objeto = None
            self.autorizacao_objeto = None
            self.current_index = 0
            self.error_window = QtWidgets.QErrorMessage()
            self.error_window.setWindowTitle("Erro!")
            self.message_window = QtWidgets.QErrorMessage()
            self.message_window.setWindowTitle("Informe")
            return
        
        #Métodos para criação de janelas

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ParkingApp"))
        self.Menu_Label_tituloParkingApp.setText(_translate("MainWindow", "Parking App"))
        self.Menu_Label_nomeUsuario.setText(_translate("MainWindow", "Nome do Usuário"))
        self.Menu_Button_pessoas.setText(_translate("MainWindow", "Pessoas"))
        self.Menu_Button_veiculos.setText(_translate("MainWindow", "Veículos"))
        self.Menu_Button_usuarios.setText(_translate("MainWindow", "Usuários"))
        self.Menu_Button_autorizacoes.setText(_translate("MainWindow", "Autorizações"))
        self.Menu_Button_logout.setText(_translate("MainWindow", "Logout"))
        self.Geral_Label_tituloDaTela.setText(_translate("MainWindow", "Título da Tela"))
        self.MenuPessoas_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.MenuPessoas_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.MenuPessoas_tab), _translate("MainWindow", "Menu Pessoas"))
        self.MenuVeiculos_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.MenuVeiculos_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.MenuVeiculos_tab), _translate("MainWindow", "Menu Veículos"))
        self.MenuUsuarios_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.MenuUsuarios_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.MenuUser_tab), _translate("MainWindow", "Menu Usuários"))
        self.MenuAutorizacoes_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.MenuAutorizacoes_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.MenuAutorizacoes_tab), _translate("MainWindow", "Menu Autorizacoes"))
        self.BuscaPessoas_Line_campo.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.BuscaPessoas_Label_buscarPor.setText(_translate("MainWindow", "Buscar por:"))
        self.BuscaPessoas_ComboBox_chaveBusca.setCurrentText(_translate("MainWindow", "Nome"))
        self.BuscaPessoas_ComboBox_chaveBusca.setItemText(0, _translate("MainWindow", "Nome"))
        self.BuscaPessoas_ComboBox_chaveBusca.setItemText(1, _translate("MainWindow", "CPF"))
        self.BuscaPessoas_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        item = self.BuscaPessoas_Table_tabelaPessoas.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Nome"))
        item = self.BuscaPessoas_Table_tabelaPessoas.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "CPF"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.BuscaPessoas_tab), _translate("MainWindow", "Busca Pessoas"))
        self.BuscaVeiculos_Line_campo.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.BuscaVeiculso_Label_buscarPor.setText(_translate("MainWindow", "Buscar por:"))
        self.BuscaVeiculos_ComboBox_chaveBusca.setCurrentText(_translate("MainWindow", "Placa"))
        self.BuscaVeiculos_ComboBox_chaveBusca.setItemText(0, _translate("MainWindow", "Placa"))
        self.BuscaVeiculos_ComboBox_chaveBusca.setItemText(1, _translate("MainWindow", "Modelo"))
        self.BuscaVeiculos_ComboBox_chaveBusca.setItemText(2, _translate("MainWindow", "Cor"))
        self.BuscaVeiculos_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        item = self.BuscaVeiculos_Table_tabelaVeiculos.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Placa"))
        item = self.BuscaVeiculos_Table_tabelaVeiculos.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Modelo"))
        item = self.BuscaVeiculos_Table_tabelaVeiculos.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Cor"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.BuscaVeiculos_tab), _translate("MainWindow", "Busca Veículos"))
        self.BuscaUsuarios_Line_campo.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.BuscaUsuarios_Label_buscarPor.setText(_translate("MainWindow", "Buscar por:"))
        self.BuscaUsuarios_ComboBox_ChaveBusca.setCurrentText(_translate("MainWindow", "Usuário"))
        self.BuscaUsuarios_ComboBox_ChaveBusca.setItemText(0, _translate("MainWindow", "Usuário"))
        self.BuscaUsuarios_ComboBox_ChaveBusca.setItemText(1, _translate("MainWindow", "Nome"))
        self.BuscaUsuarios_ComboBox_ChaveBusca.setItemText(2, _translate("MainWindow", "CPF"))
        self.BuscaUsuarios_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        item = self.BuscaUsuarios_Table_tabelaUsuarios.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Usuário"))
        item = self.BuscaUsuarios_Table_tabelaUsuarios.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Nome"))
        item = self.BuscaUsuarios_Table_tabelaUsuarios.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Tipo"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.BuscUser_tab), _translate("MainWindow", "Busca Usuários"))
        self.BuscaAutorizacoes_Line_campoBusca.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.BuscaAutorizacoes_Label_buscarPor.setText(_translate("MainWindow", "Buscar por:"))
        self.BuscaAutorizacoes_ComboBox_chaveBusca.setCurrentText(_translate("MainWindow", "Nome"))
        self.BuscaAutorizacoes_ComboBox_chaveBusca.setItemText(0, _translate("MainWindow", "Nome"))
        self.BuscaAutorizacoes_ComboBox_chaveBusca.setItemText(1, _translate("MainWindow", "CPF"))
        self.BuscaAutorizacoes_ComboBox_chaveBusca.setItemText(2, _translate("MainWindow", "Placa"))
        self.BuscaAutorizacoes_Button_buscar.setText(_translate("MainWindow", "Buscar"))
        item = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Nome"))
        item = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "CPF"))
        item = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Placa"))
        item = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Modelo"))
        item = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.horizontalHeaderItem(4)
        item.setText(_translate("MainWindow", "Cor"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.BuscaAutorizacoes_tab), _translate("MainWindow", "Busca Autorizações"))
        self.CadastroPessoa_Label_nome.setText(_translate("MainWindow", "Nome: "))
        self.CadastroPessoa_Line_nome.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroPessoa_Label_CPF.setText(_translate("MainWindow", "CPF: "))
        self.CadastroPessoa_Line_CPF.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroPessoa_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.CadastroPessoa_tab), _translate("MainWindow", "Cadastro Pessoas"))
        self.CadastroVeiculos_Label_placa.setText(_translate("MainWindow", "Placa: "))
        self.CadastroVeiculos_Line_placa.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroVeiculos_Label_modelo.setText(_translate("MainWindow", "Modelo: "))
        self.CadastroVeiculos_Line_modelo.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroVeiculos_Line_cor.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroVeiculos_Label_cor.setText(_translate("MainWindow", "Cor: "))
        self.CadastroVeiculo_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.tab), _translate("MainWindow", "Cadastro Veículos"))
        self.CadastroUser_Label_pessoa.setText(_translate("MainWindow", "Pessoa: "))
        self.CadastroUser_Line_pessoa.setPlaceholderText(_translate("MainWindow", "Use o botão ao lado para buscar"))
        self.CadastroUser_Button_pessoaBuscar.setText(_translate("MainWindow", "Buscar"))
        self.CadastroUser_Label_user.setText(_translate("MainWindow", "Usuário: "))
        self.CadastroUser_Line_usuario.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroUser_Label_senha.setText(_translate("MainWindow", "Senha: "))
        self.CadastroUser_Line_senha.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroUser_Label_confSenha.setText(_translate("MainWindow", "Confirmar Senha: "))
        self.CadastroUser_Line_confirmarSenha.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroUser_Label_tipoUser.setText(_translate("MainWindow", "Tipo de Usuário:"))
        self.CadastroUser_ComboBox_tipoUsser.setItemText(0, _translate("MainWindow", "Administrador"))
        self.CadastroUser_ComboBox_tipoUsser.setItemText(1, _translate("MainWindow", "Secretário"))
        self.CadastroUser_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.CadUser_tab), _translate("MainWindow", "Cadastro Usuários"))
        self.CadastroAutorizacao_Label_pessoa.setText(_translate("MainWindow", "Pessoa: "))
        self.CadastroAuto_Line_pessoa.setPlaceholderText(_translate("MainWindow", "Use o botão ao lado para buscar"))
        self.CadastroAutorizacao_Button_pessoaBuscar.setText(_translate("MainWindow", "Buscar"))
        self.CadastroAutorizacao_Label_veiculo.setText(_translate("MainWindow", "Veículo: "))
        self.CadastroAuto_Line_veiculo.setPlaceholderText(_translate("MainWindow", "Use o botão ao lado para buscar"))
        self.CadastroAutorizacao_Button_veiculoBuscar.setText(_translate("MainWindow", "Buscar"))
        self.CadastroAutorizacao_Label_validade.setText(_translate("MainWindow", "Validade: "))
        self.CadastroAuto_Line_validade.setPlaceholderText(_translate("MainWindow", "xx/xx/xxxx"))
        self.CadastroAuto_Label_descricao.setText(_translate("MainWindow", "Descrição: "))
        self.CadastroAuto_TextBox_descricao.setPlaceholderText(_translate("MainWindow", "Digite aqui..."))
        self.CadastroAutorizacao_Button_cadastrar.setText(_translate("MainWindow", "Cadastrar"))
        self.tab_telas.setTabText(self.tab_telas.indexOf(self.CadastroAutorizacao_tab), _translate("MainWindow", "Cadastro Autorização"))
        
        self.initCustomConfig() 
     
    def setSignalsSlots(self):
        #Indexes
        """
        0 - menu de pessoas
        1 - menu  de veículos
        2 - menu de usuários
        3 - menu de autorizações
        4 - busca de pessoas
        5 - busca de veiculos
        6 - busca de usuários
        7 - busca de autorizações
        8 - cadastro de pessoas
        9 - cadastro de veiculos
        10 - cadastro de usuarios
        11 - cadastro de autorizações
        """
        
        #Menus
        self.Menu_Button_pessoas.clicked.connect(lambda: self.mudarTela(0))
        self.Menu_Button_veiculos.clicked.connect(lambda: self.mudarTela(1))
        self.Menu_Button_usuarios.clicked.connect(lambda: self.mudarTela(2))
        self.Menu_Button_autorizacoes.clicked.connect(lambda: self.mudarTela(3))
        
        #Buscas
        self.MenuPessoas_Button_buscar.clicked.connect(lambda: self.mudarTela(4))
        self.MenuVeiculos_Button_buscar.clicked.connect(lambda: self.mudarTela(5))
        self.MenuUsuarios_Button_buscar.clicked.connect(lambda: self.mudarTela(6))
        self.MenuAutorizacoes_Button_buscar.clicked.connect(lambda: self.mudarTela(7))

        #Cadastros
        self.MenuPessoas_Button_cadastrar.clicked.connect(lambda: self.mudarTela(8))
        self.MenuVeiculos_Button_cadastrar.clicked.connect(lambda: self.mudarTela(9))
        self.MenuUsuarios_Button_cadastrar.clicked.connect(lambda: self.mudarTela(10))
        self.MenuAutorizacoes_Button_cadastrar.clicked.connect(lambda: self.mudarTela(11))

        #Botões de Busca
        self.BuscaPessoas_Button_buscar.clicked.connect(lambda: self.buscarPessoas())        
        self.BuscaVeiculos_Button_buscar.clicked.connect(lambda: self.buscarVeiculos()) 
        self.BuscaUsuarios_Button_buscar.clicked.connect(lambda: self.buscarUsuarios()) 
        self.BuscaAutorizacoes_Button_buscar.clicked.connect(lambda: self.buscarAutorizacoes())
        
        #Novas Janelas
        self.CadastroAutorizacao_Button_pessoaBuscar.clicked.connect(lambda: self.criarJanelaBuscaPessoas(self.CadastroAuto_Line_pessoa))
        self.CadastroAutorizacao_Button_veiculoBuscar.clicked.connect(lambda: self.criarJanelaBuscaVeiculos(self.CadastroAuto_Line_veiculo))
        self.CadastroUser_Button_pessoaBuscar.clicked.connect(lambda: self.criarJanelaBuscaPessoas(self.CadastroUser_Line_pessoa))

        #Confirmações
        self.CadastroAutorizacao_Button_cadastrar.clicked.connect(lambda: self.cadastrarAutorizacao())
        self.CadastroPessoa_Button_cadastrar.clicked.connect(lambda: self.cadastrarPessoa())
        self.CadastroVeiculo_Button_cadastrar.clicked.connect(lambda: self.cadastrarVeiculo())
        self.CadastroUser_Button_cadastrar.clicked.connect(lambda: self.cadastrarUsuario())
        
    ## TRANSIÇÃO ##

    def mudarTela(self, index):
            #Indexes
            """
            0 - menu usuarios
            1 - menu  motorista
            2 - busca usuarios
            3 - busca motoristas
            4 - cadastro usuarios
            5 - cadastro motoristas
            """
            current_index = self.tab_telas.currentIndex()
            self.tab_telas.setTabEnabled(current_index, False)
            self.tab_telas.setCurrentIndex(index)
            self.tab_telas.setTabEnabled(index, True)
            self.Geral_Label_tituloDaTela.setText(getNomeTela(index))

    def esconderBotoesAutorizacao(self, auth):
        if(auth == 3):
            """ Fazer alterações de porteiro """
            print("porteiro")
            self.MenuPessoas_Button_cadastrar.hide()
            self.MenuVeiculos_Button_cadastrar.hide()
            self.Menu_Button_usuarios.hide()
            self.MenuAutorizacoes_Button_cadastrar.hide()
            self.Menu_Label_nomeUsuario.setText("Porteiro logado")
        if(auth == 2):
            """ Fazer alterações de secretário """
            self.Menu_Button_usuarios.hide()
        if(auth == 1):
            """ Não fazer alterações """
        else:
            """ Negar acesso """
            print("Usuário inválido.")
            self.hide()
           


    ## POP-UPs DE BUSCA ##

    def getPessoa(self, pessoa, linha):
        if(pessoa is None):
            aviso = "Não é possível buscar. Campo de busca vazio!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1

        self.pessoa_objeto = pessoa
        linha.setText(pessoa.pes_nm_pessoa)
        return
        
    
    
    def getVeiculo(self, veiculo, linha):
        if(veiculo is None):
            aviso = "Não é possível buscar. Campo de busca vazio!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1

        self.veiculo_objeto = veiculo
        linha.setText(veiculo.vec_nu_placa)     
        return

    def criarJanelaBuscaPessoas(self, line):
        dialogBuscaPessoas = janelaPessoa()
        dialogBuscaPessoas.setupUi(dialogBuscaPessoas)
        
        dialogBuscaPessoas.BuscaPessoa_ButtonBox_Confirmacao.accepted.connect(lambda: self.getPessoa(dialogBuscaPessoas.getPessoaSelecionada(), line))
        dialogBuscaPessoas.BuscaPessoa_ButtonBox_Confirmacao.rejected.connect(lambda: print("rejeitado"))
        
        dialogBuscaPessoas.exec_()
        return
    
    def criarJanelaBuscaVeiculos(self, line):
        dialogBuscaVeiculos = janelaVeiculo()
        dialogBuscaVeiculos.setupUi(dialogBuscaVeiculos)
        
        dialogBuscaVeiculos.BuscaVeiculo_ButtonBox_Confirmacao.accepted.connect(lambda: self.getVeiculo(dialogBuscaVeiculos.getVeiculoSelecionado(), line))
        dialogBuscaVeiculos.BuscaVeiculo_ButtonBox_Confirmacao.rejected.connect(lambda: print("rejeitado"))
        
        dialogBuscaVeiculos.exec_()
        return



    ## MÉTODOS DE BUSCA ##

    def buscarPessoas(self):
        if(self.BuscaPessoas_Line_campo.text() == ""):
            aviso = "Não é possível buscar. Campo de busca vazio!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1
        #
        #   LÓGICA DO PessoaDAO
        #
        pessoaDAO = PessoaDAO()
        chaveBusca = self.BuscaPessoas_Line_campo.text()
        pessoas = list()

        # Se for por nome
        if(self.BuscaPessoas_ComboBox_chaveBusca.currentIndex() == 0):
            pessoas.extend(pessoaDAO.getQuery(pes_nm_pessoa='%'+chaveBusca+'%'))
            if(len(pessoas) == 0):
                aviso = "Não existe pessoa registrada com o nome informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        # Se não por CPF
        else:
            pessoas.extend(pessoaDAO.getQuery(pes_cpf_pessoa=chaveBusca))
            if(len(pessoas) == 0):
                aviso = "Não existe pessoa registrada com o CPF informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        self.populaTabelaPessoas(pessoas)
        return

    def buscarVeiculos(self):
        if(self.BuscaVeiculos_Line_campo.text() == ""):
            aviso = "Não é possível buscar. Campo de busca vazio!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1
        #
        #   LÓGICA DO VeiculoDAO
        #
        veiculoDAO = VeiculoDAO()
        chaveBusca = self.BuscaVeiculos_Line_campo.text()
        veiculos = list()

        # Se for pela placa
        if(self.BuscaVeiculos_ComboBox_chaveBusca.currentIndex() == 0):
            veiculos.extend(veiculoDAO.getQuery(vec_nu_placa=chaveBusca))
            if(len(veiculos) == 0):
                aviso = "Não existe veículo registrado com a placa informada!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        # Se não, pelo modelo
        elif(self.BuscaVeiculos_ComboBox_chaveBusca.currentIndex() == 1):
            veiculos.extend(veiculoDAO.getQuery(vec_ds_modelo='%'+chaveBusca+'%'))
            if(len(veiculos) == 0):
                aviso = "Não existe veículo registrado com o modelo informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        # Se não, cor
        else:
            veiculos.extend(veiculoDAO.getQuery(vec_ds_cor='%'+chaveBusca+'%'))
            if(len(veiculos) == 0):
                aviso = "Não existe veículo registrado com a cor informada!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        self.populaTabelaVeiculos(veiculos)
        return 0

    def buscarAutorizacoes(self):
        if(self.BuscaAutorizacoes_Line_campoBusca.text() == ""):
            aviso = "Não é possível buscar. Campo de busca vazio!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1
        #
        #   LÓGICA Do AutorizacaoDAO
        #
        autorizacaoDAO = AutorizacaoDAO()
        pessoaDAO = PessoaDAO()
        veiculoDAO = VeiculoDAO()
        chaveBusca = self.BuscaAutorizacoes_Line_campoBusca.text()
        autorizacoes = list()

        # Se for pelo nome
        if(self.BuscaAutorizacoes_ComboBox_chaveBusca.currentIndex() == 0):
            pessoas = pessoaDAO.getQuery(pes_nm_pessoa='%' + chaveBusca + '%')
            
            if(len(pessoas) == 0):
                aviso = "Não existe pessoa registrada com o nome informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

            for p in pessoas:
                autorizacoes.extend(autorizacaoDAO.getQuery(aut_id_pessoa=p.pes_id_pessoa))

        # Se não, pelo CPF
        elif(self.BuscaAutorizacoes_ComboBox_chaveBusca.currentIndex() == 1):
            pessoas = pessoaDAO.getQuery(pes_cpf_pessoa=chaveBusca)

            if(len(pessoas) == 0):
                aviso = "Não existe pessoa registrada com o CPF informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

            # CPF é UNIQUE
            autorizacoes.extend(autorizacaoDAO.getQuery(aut_id_pessoa=pessoas[0].pes_id_pessoa))

        # Se não, pela placa
        else:
            veiculos = veiculoDAO.getQuery(vec_nu_placa=chaveBusca)
            
            if(len(veiculos) == 0):
                aviso = "Não existe veículo registrado com a placa informada!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

            # Placa é UNIQUE
            autorizacoes.extend(autorizacaoDAO.getQuery(aut_id_veiculo=veiculos[0].vec_id_veiculo))

        if(len(autorizacoes) == 0):
            aviso = "Sua busca não retornou resultados!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1

        self.populaTabelaAutorizacoes(autorizacoes)
        return 0


    def buscarUsuarios(self):
        if(self.BuscaUsuarios_Line_campo.text() == ""):
            text = "Não é possível buscar. Campo de busca vazio!"
            print(text)
            self.error_window.showMessage(text)
            return
        #
        #   LÓGICA DO veiculoDAO
        #
        usuarioDAO = UsuarioDAO()
        pessoaDAO = PessoaDAO()
        chaveBusca = self.BuscaUsuarios_Line_campo.text()
        usuarios = list()

        # Se for pelo nome de usuário
        if(self.BuscaUsuarios_ComboBox_ChaveBusca.currentIndex() == 0):
            usuarios = usuarioDAO.getQuery(usr_ds_nickname='%' + chaveBusca + '%')

            if(len(usuarios) == 0):
                aviso = "Não existe usuário registrado com o nome informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

        # Se for pelo nome da pessoa
        if(self.BuscaUsuarios_ComboBox_ChaveBusca.currentIndex() == 1):
            pessoas = pessoaDAO.getQuery(pes_nm_pessoa='%' + chaveBusca + "%")
        
            if(len(pessoas) == 0):
                aviso = "Não existe usuário registrado para a pessoa informada!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1
            
            for p in pessoas:
                usuarios.extend(usuarioDAO.getQuery(usr_id_pessoa=p.pes_id_pessoa))
        
        # Se for pelo cpf da pessoa
        if(self.BuscaUsuarios_ComboBox_ChaveBusca.currentIndex() ==2):
            pessoas = pessoaDAO.getQuery(pes_cpf_pessoa=chaveBusca)

            if(len(pessoas) == 0):
                aviso = "Não existe usuário registrado para o cpf informado!"
                print(aviso)
                self.error_window.showMessage(aviso)
                return -1

            # CPF é UNIQUE
            usuarios.extend(usuarioDAO.getQuery(usr_id_pessoa=pessoas[0].pes_id_pessoa))

        if(len(usuarios) == 0):
            aviso = "Sua busca não retornou resultados!"
            print(aviso)
            self.error_window.showMessage(aviso)
            return -1

        self.populaTabelaUsuarios(usuarios)
        return 0


    
    ## MÉTODOS DE CADASTRO ##
    def cadastrarUsuario(self):
        erro_flag = False
        erro_text =  ""
        if(self.pessoa_objeto == None):
            erro_flag = True
            erro_text = erro_text + "Campo pessoa não preenchido."
        if(self.CadastroUser_Line_usuario.text() == ""):
            erro_flag = True
            erro_text = erro_text + "\nCampo usuário não preenchido."
        if(self.CadastroUser_Line_senha.text() == ""):
            erro_flag = True
            erro_text = erro_text + "\nCampo senha não preenchido."
        if(self.CadastroUser_Line_confirmarSenha.text() == ""):
            erro_flag = True
            erro_text = erro_text + "\nCampo confirmar senha não preenchido."
        if(self.CadastroUser_Line_senha.text() != self.CadastroUser_Line_confirmarSenha.text() and erro_flag == False):
            erro_flag = True
            erro_text = erro_text + "\nCampos senha e confirmação de senha não coincidem."

        if(erro_flag):
            self.error_window.showMessage(erro_text)
            erro_flag = False
        else:
            try:
                usuarioDAO = UsuarioDAO()
                # Verificando se a pessoa selecionada já possui usuário cadastrado no banco
                if(len(usuarioDAO.getQuery(usr_id_pessoa=self.pessoa_objeto.pes_id_pessoa)) != 0):
                    erro_flag = True
                    erro_text = "\n Pessoa selecionada já possui usuário! Uma pessoa não pode ter mais de um usuário vinculado."
                # Verificando se nome de usuário já existe
                if(len(usuarioDAO.getQuery(usr_ds_nickname=self.CadastroUser_Line_usuario.text())) != 0 ):
                    erro_flag = True
                    erro_text = erro_text + "\n\n Nome de usuário já utilizado! Por favor, escolha outro."

                if(erro_flag):
                    self.error_window.showMessage(erro_text)
                    erro_flag = False                   
                else:
                    usuario = Usuario()
                    usuario.usr_id_pessoa = self.pessoa_objeto.pes_id_pessoa
                    usuario.usr_ds_nickname = self.CadastroUser_Line_usuario.text()
                    # O index atual selecionado na combobox é o (index + 1) do tipo usuário na base
                    usuario.usr_id_tipo_usuario = self.CadastroUser_ComboBox_tipoUsser.currentIndex() + 1
                        
                    # Armazenando a senha inserida como hash
                    autenticacao = Autenticacao()
                    usuario.usr_ds_senha = autenticacao.genpass(self.CadastroUser_Line_senha.text())

                    usuarioDAO.insert(usuario)
                    # JANELA DE SUCESSO#
                    self.message_window.showMessage("Operação concluída com sucesso!")

                    # LIMPANDO CAMPOS #
                    self.pessoa_objeto = None
                    self.CadastroUser_Line_pessoa.setText("")
                    self.CadastroUser_Line_usuario.setText("")
                    self.CadastroUser_Line_senha.setText("")
                    self.CadastroUser_Line_confirmarSenha.setText("")
                    self.CadastroUser_ComboBox_tipoUsser.setCurrentIndex(0)

            except Exception as e:
                print(e)

    def cadastrarAutorizacao(self):
        erro_flag = False
        erro_text =  ""
        if(self.pessoa_objeto == None):
            erro_flag = True
            erro_text = erro_text + "Campo pessoa não preenchido."
        if(self.veiculo_objeto == None):
            erro_text = erro_text + "\nCampo veículo não preenchido."
        if(erro_flag):
            self.error_window.showMessage(erro_text)
            erro_flag = False
        else:
            try:
                autorizacao = Autorizacao()
                autorizacao.aut_id_pessoa = self.pessoa_objeto.pes_id_pessoa
                autorizacao.aut_id_veiculo = self.veiculo_objeto.vec_id_veiculo
                autorizacao.aut_ds_autorizacao = self.CadastroAuto_TextBox_descricao.toPlainText()
                autorizacao.aut_dt_expiracao = StringToDate(self.CadastroAuto_Line_validade.text())
                autorizacao.aut_dt_cadastro = datetime.date.today()
                autorizacaoDAO = AutorizacaoDAO()
                autorizacaoDAO.insert(autorizacao)
            except Exception as e:
                print(e)

            # JANELA DE SUCESSO#
            self.message_window.showMessage("Operação concluída com sucesso!")

            # LIMPANDO CAMPOS #
            self.CadastroAuto_Line_pessoa.setText("")
            self.CadastroAuto_Line_veiculo.setText("")
            self.pessoa_objeto = None
            self.veiculo_objeto = None
            self.CadastroAuto_Line_validade.setText("")
            self.CadastroAuto_TextBox_descricao.setText("")

    def cadastrarPessoa(self):
        erro_flag = False
        erro_text = ""
        if(self.CadastroPessoa_Line_nome.text() == ""):
            erro_flag = True
            erro_text = erro_text + "Campo Nome não preenchido."
        if(erro_flag):
            self.error_window.showMessage(erro_text)
            erro_flag = False
        else:
            try:
                pessoa = Pessoa()
                pessoa.pes_nm_pessoa = self.CadastroPessoa_Line_nome.text()
                pessoa.pes_cpf_pessoa = self.CadastroPessoa_Line_CPF.text()
                pessoaDAO = PessoaDAO()
                pessoaDAO.insert(pessoa)
              
                # JANELA DE SUCESSO#
                self.message_window.showMessage("Operação concluída com sucesso!")

                # LIMPANDO CAMPOS #
                self.CadastroPessoa_Line_nome.setText("")
                self.CadastroPessoa_Line_CPF.setText("")
            except Exception as e:
                print(e)

    def cadastrarVeiculo(self):
        erro_flag = False
        erro_text = ""
        if(self.CadastroVeiculos_Line_placa.text() == ""):
            erro_flag = True
            erro_text = erro_text + "Campo Placa não preenchido."
        if(erro_flag):
            self.error_window.showMessage(erro_text)
            erro_flag = False
        else:
            try:
                veiculo = Veiculo()
                veiculo.vec_nu_placa = self.CadastroVeiculos_Line_placa.text()
                veiculo.vec_ds_modelo = self.CadastroVeiculos_Line_modelo.text()
                veiculo.vec_ds_cor = self.CadastroVeiculos_Line_cor.text()
                veiculoDAO = VeiculoDAO()
                veiculoDAO.insert(veiculo)

                # JANELA DE SUCESSO#
                self.message_window.showMessage("Operação concluída com sucesso!")

                # LIMPANDO CAMPOS #
                self.CadastroVeiculos_Line_placa.setText("")
                self.CadastroVeiculos_Line_modelo.setText("")
                self.CadastroVeiculos_Line_cor.setText("")

            except Exception as e:
                print(e)


    ## MÉTODOS DE POPULAÇÃO ##

    def populaTabelaPessoas(self, pessoas):
        self.BuscaPessoas_Table_tabelaPessoas.setRowCount(0)
        for p in pessoas:
            posicaoLinha = self.BuscaPessoas_Table_tabelaPessoas.rowCount()
            self.BuscaPessoas_Table_tabelaPessoas.insertRow(posicaoLinha)
            self.BuscaPessoas_Table_tabelaPessoas.setItem(posicaoLinha, 0 , QtWidgets.QTableWidgetItem(p.pes_nm_pessoa))
            self.BuscaPessoas_Table_tabelaPessoas.setItem(posicaoLinha, 1, QtWidgets.QTableWidgetItem(p.pes_cpf_pessoa))

    def populaTabelaVeiculos(self, veiculos):
        self.BuscaVeiculos_Table_tabelaVeiculos.setRowCount(0)
        for v in veiculos:
            posicaoLinha = self.BuscaVeiculos_Table_tabelaVeiculos.rowCount()
            self.BuscaVeiculos_Table_tabelaVeiculos.insertRow(posicaoLinha)
            self.BuscaVeiculos_Table_tabelaVeiculos.setItem(posicaoLinha, 0 , QtWidgets.QTableWidgetItem(v.vec_nu_placa))
            self.BuscaVeiculos_Table_tabelaVeiculos.setItem(posicaoLinha, 1, QtWidgets.QTableWidgetItem(v.vec_ds_modelo))
            self.BuscaVeiculos_Table_tabelaVeiculos.setItem(posicaoLinha, 2, QtWidgets.QTableWidgetItem(v.vec_ds_cor))

    def populaTabelaAutorizacoes(self, autorizacoes):
        self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setRowCount(0)
        for a in autorizacoes:
            posicaoLinha = self.BuscaAutorizacoes_Table_tabelaAutorizacoes.rowCount()
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.insertRow(posicaoLinha)
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setItem(posicaoLinha, 0 , QtWidgets.QTableWidgetItem(a.PES_PESSOA.pes_nm_pessoa))
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setItem(posicaoLinha, 1, QtWidgets.QTableWidgetItem(a.PES_PESSOA.pes_cpf_pessoa))
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setItem(posicaoLinha, 2, QtWidgets.QTableWidgetItem(a.VEC_VEICULO.vec_nu_placa))
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setItem(posicaoLinha, 3, QtWidgets.QTableWidgetItem(a.VEC_VEICULO.vec_ds_modelo))
            self.BuscaAutorizacoes_Table_tabelaAutorizacoes.setItem(posicaoLinha, 4, QtWidgets.QTableWidgetItem(a.VEC_VEICULO.vec_ds_cor))

    def populaTabelaUsuarios(self, usuarios):
        self.BuscaUsuarios_Table_tabelaUsuarios.setRowCount(0)
        for u in usuarios:
            posicaoLinha = self.BuscaUsuarios_Table_tabelaUsuarios.rowCount()
            self.BuscaUsuarios_Table_tabelaUsuarios.insertRow(posicaoLinha)
            self.BuscaUsuarios_Table_tabelaUsuarios.setItem(posicaoLinha, 0, QtWidgets.QTableWidgetItem(u.usr_ds_nickname))
            self.BuscaUsuarios_Table_tabelaUsuarios.setItem(posicaoLinha, 1, QtWidgets.QTableWidgetItem(u.PES_PESSOA.pes_nm_pessoa))
            self.BuscaUsuarios_Table_tabelaUsuarios.setItem(posicaoLinha, 2, QtWidgets.QTableWidgetItem('Administrador' if (u.TIU_TIPO_USUARIO.tiu_ch_administrador == 1) else 'Secretário'))


if __name__ == "__main__":
    app = QtWidgets.QApplication.instance()
    if app is None:
        app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui2 = Ui_MainWindow()
    ui2.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

