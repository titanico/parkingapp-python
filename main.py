import GUI_ParkingApp
from imports.logout import logout
from Login_GUI import Ui_Login
from GUI_ParkingApp import Ui_MainWindow
from PyQt5 import QtWidgets
import sys

if __name__ == "__main__":
    
    #Criação dos objetos para as tela de login e GUI principal
    app = QtWidgets.QApplication(sys.argv)
    if app is None:
        app = QtWidgets.QApplication(sys.argv)
    login = Ui_Login()
    login.setupUi(login)
    
    #Checagem se o login foi executado com sucesso.
    if login.exec_() == QtWidgets.QDialog.Accepted:
        main_window = Ui_MainWindow()
        main_window.setupUi(main_window)
        #Conexão do botão de logout com a função de logout.
        main_window.Menu_Button_logout.clicked.connect(logout)
        main_window.esconderBotoesAutorizacao(login.USER_AUTH)
        main_window.mudarTela(0)
        main_window.show()
        sys.exit(app.exec_())