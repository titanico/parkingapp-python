from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QDialog
from imports.PessoaDAO import PessoaDAO
from db.model import PESPESSOA as Pessoa

# Herda de QDialog em vez de object(Permitir fechar janela)
class Ui_Dialog(QtWidgets.QDialog):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(482, 368)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 5, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 0, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 5, 2, 1, 1)
        self.BuscaPessoa_ButtonBox_Confirmacao = QtWidgets.QDialogButtonBox(Dialog)
        self.BuscaPessoa_ButtonBox_Confirmacao.setOrientation(QtCore.Qt.Horizontal)
        self.BuscaPessoa_ButtonBox_Confirmacao.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.BuscaPessoa_ButtonBox_Confirmacao.setObjectName("BuscaPessoa_ButtonBox_Confirmacao")
        self.gridLayout.addWidget(self.BuscaPessoa_ButtonBox_Confirmacao, 5, 1, 1, 1)
        self.BuscaPessoa_Button_Pesquisar = QtWidgets.QPushButton(Dialog)
        self.BuscaPessoa_Button_Pesquisar.setObjectName("BuscaPessoa_Button_Pesquisar")
        self.gridLayout.addWidget(self.BuscaPessoa_Button_Pesquisar, 0, 2, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem3, 4, 1, 1, 1)
        self.BuscaPessoa_Line_nomePessoa = QtWidgets.QLineEdit(Dialog)
        self.BuscaPessoa_Line_nomePessoa.setObjectName("BuscaPessoa_Line_nomePessoa")
        self.gridLayout.addWidget(self.BuscaPessoa_Line_nomePessoa, 0, 0, 1, 2)
        self.BuscaPessoa_Table_Pessoas = QtWidgets.QTableWidget(Dialog)
        self.BuscaPessoa_Table_Pessoas.setAutoScroll(True)
        self.BuscaPessoa_Table_Pessoas.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.BuscaPessoa_Table_Pessoas.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.BuscaPessoa_Table_Pessoas.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.BuscaPessoa_Table_Pessoas.setObjectName("BuscaPessoa_Table_Pessoas")
        self.BuscaPessoa_Table_Pessoas.setColumnCount(3)
        self.BuscaPessoa_Table_Pessoas.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaPessoa_Table_Pessoas.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaPessoa_Table_Pessoas.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.BuscaPessoa_Table_Pessoas.setHorizontalHeaderItem(2, item)
        self.gridLayout.addWidget(self.BuscaPessoa_Table_Pessoas, 2, 0, 2, 3)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem4, 4, 0, 1, 1)
        spacerItem5 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem5, 4, 2, 1, 1)
        spacerItem6 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem6, 1, 2, 1, 1)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem7, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Busca de Pessoas"))
        self.BuscaPessoa_Button_Pesquisar.setText(_translate("Dialog", "Pesquisar"))
        self.BuscaPessoa_Line_nomePessoa.setPlaceholderText(_translate("Dialog", "Digite o nome da pessoa"))
        item = self.BuscaPessoa_Table_Pessoas.horizontalHeaderItem(0)
        item.setText(_translate("Dialog", "Id"))
        item = self.BuscaPessoa_Table_Pessoas.horizontalHeaderItem(1)
        item.setText(_translate("Dialog", "Nome"))
        item = self.BuscaPessoa_Table_Pessoas.horizontalHeaderItem(2)
        item.setText(_translate("Dialog", "CPF"))

        #Configurações personalizadas
        self.initCustomConfig()

    def initCustomConfig(self):
        self.BuscaPessoa_Table_Pessoas.setColumnHidden(0, True) #Ocultando coluna de ID
        self.setSignalsSlots()

    #TODO: Fechar QDialog ao clicar em botão Cancel
    #TODO: Fechar QDialog ao clicar em botão Ok, retornando pessoa
    def setSignalsSlots(self):
        self.BuscaPessoa_Button_Pesquisar.clicked.connect(lambda: self.populaTabela())
        self.BuscaPessoa_ButtonBox_Confirmacao.buttons()[0].clicked.connect(lambda: self.getPessoaSelecionada())
        self.BuscaPessoa_ButtonBox_Confirmacao.buttons()[1].clicked.connect(lambda: self.reject())

    def populaTabela(self):
        self.BuscaPessoa_Table_Pessoas.setRowCount(0)
        pessoas = self.buscaPessoa()

        for p in pessoas:
            posicaoLinha = self.BuscaPessoa_Table_Pessoas.rowCount()
            self.BuscaPessoa_Table_Pessoas.insertRow(posicaoLinha)
            self.BuscaPessoa_Table_Pessoas.setItem(posicaoLinha, 0 , QtWidgets.QTableWidgetItem(str(p.pes_id_pessoa)))
            self.BuscaPessoa_Table_Pessoas.setItem(posicaoLinha, 1 , QtWidgets.QTableWidgetItem(p.pes_nm_pessoa))
            self.BuscaPessoa_Table_Pessoas.setItem(posicaoLinha, 2, QtWidgets.QTableWidgetItem(p.pes_cpf_pessoa))

    def buscaPessoa(self):
        pessoaDAO = PessoaDAO()
        nomePessoa = self.BuscaPessoa_Line_nomePessoa.text()
        return pessoaDAO.getQuery(pes_nm_pessoa='%'+nomePessoa+'%')

    def getPessoaSelecionada(self):
        if(self.BuscaPessoa_Table_Pessoas.rowCount() > 0):
            if(len(self.BuscaPessoa_Table_Pessoas.selectedItems()) > 0):
                nome = self.BuscaPessoa_Table_Pessoas.selectedItems()[0]
                cpf = self.BuscaPessoa_Table_Pessoas.selectedItems()[1]
                pessoa = Pessoa()
                pessoa.pes_id_pessoa = int(self.BuscaPessoa_Table_Pessoas.item(nome.row(),0).text(), base=10)
                pessoa.pes_cpf_pessoa = cpf.text()
                pessoa.pes_nm_pessoa = nome.text()
                self.accept()
                # #DEBUG#
                # print("ID: " + self.BuscaPessoa_Table_Pessoas.item(nome.row(),0).text())
                # print("Pessoa: " + nome.text())
                # print("CPF: " + cpf.text())
                # #DEBUG#
                return pessoa
            else:
                self.reject()
                #DEBUG#
                print("Retornando nada! Nenhuma pessoa selecionada.")
                #DEBUG#
                return None

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_Dialog()
    ui.setupUi(ui)
    ui.show()
    sys.exit(app.exec_())


