from imports.UsuarioDAO import UsuarioDAO
from db.model import USRUSUARIO as User
from imports.login import login
from imports.auth import authentication
from etc.make_pepper import generatePepper

senha = input("Informe uma senha para o usuário Administrador: ")
ip_servidor_banco_dados = input("Informe o IP do servidor do banco de dados da aplicação: ")

configFile = open("/etc/ParkingApp.conf","w")
print(ip_servidor_banco_dados,file=configFile)
configFile.close()

print("Gerando chave...")
generatePepper()

usrDAO = UsuarioDAO()
auth = authentication()
print("Gerando Hash para senha do usuário admin")
admin = usrDAO.getUsuario(usr_id_usuario=1)
usrDAO.update(admin,"usr_ds_senha",auth.genpass(senha))
