from PyQt5 import QtCore, QtGui, QtWidgets
from imports.login import login

class Ui_Login(QtWidgets.QDialog):
    def inicializarSignalsSlots(self):
        self.buttonEntrar.clicked.connect(self.tentarLogin)
        self.buttonPorteiro.clicked.connect(self.porteiroLogin)
        return
        
    def porteiroLogin(self):
        self.USER_AUTH = 3
        print("Logando como porteiro")
        self.accept()
        
    def tentarLogin(self):
        login_obj = login(self.lineUser.text(),self.linePassword.text())
        if(login_obj.validate()):
            #Alteração do atributo interno. Será passado para a tela principal no main.py.
            self.USER_AUTH = login_obj.user.usr_id_tipo_usuario
            print("Logado como",self.lineUser.text())
            self.accept()
            return
        else:
            text = "Usuário ou senha incorretos."
            self.error_window.showMessage(text)
            return
        
    def setupUi(self, Login):
        Login.setObjectName("Login")
        Login.resize(220, 290)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(Login)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.viewLogin = QtWidgets.QGraphicsView(Login)
        self.viewLogin.setObjectName("viewLogin")
        self.verticalLayout.addWidget(self.viewLogin)
        self.line = QtWidgets.QFrame(Login)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.label = QtWidgets.QLabel(Login)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.lineUser = QtWidgets.QLineEdit(Login)
        self.lineUser.setObjectName("lineUser")
        self.verticalLayout.addWidget(self.lineUser)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.label_2 = QtWidgets.QLabel(Login)
        self.label_2.setObjectName("label_2")
        self.verticalLayout_2.addWidget(self.label_2)
        self.linePassword = QtWidgets.QLineEdit(Login)
        self.linePassword.setObjectName("linePassword")
        self.linePassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.verticalLayout_2.addWidget(self.linePassword)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.buttonEntrar = QtWidgets.QPushButton(Login)
        self.buttonEntrar.setObjectName("buttonEntrar")
        self.horizontalLayout.addWidget(self.buttonEntrar)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem2)
        self.verticalLayout_2.addLayout(self.horizontalLayout)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.buttonPorteiro = QtWidgets.QPushButton(Login)
        self.buttonPorteiro.setObjectName("buttonPorteiro")
        self.horizontalLayout_2.addWidget(self.buttonPorteiro)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem4)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.verticalLayout_4.addLayout(self.verticalLayout_3)

        self.retranslateUi(Login)
        self.inicializarSignalsSlots()
        QtCore.QMetaObject.connectSlotsByName(Login)
        self.error_window = QtWidgets.QErrorMessage()
        self.error_window.setWindowTitle("Erro!")

    def retranslateUi(self, Login):
        _translate = QtCore.QCoreApplication.translate
        Login.setWindowTitle(_translate("Login", "Login - ParkingApp"))
        self.label.setText(_translate("Login", "Usuário:"))
        self.lineUser.setPlaceholderText(_translate("Login", "Digite seu usuário..."))
        self.label_2.setText(_translate("Login", "Senha:"))
        self.linePassword.setPlaceholderText(_translate("Login", "Digite sua senha..."))
        self.buttonEntrar.setText(_translate("Login", "Entrar"))
        self.buttonPorteiro.setText(_translate("Login", "Entrar como Porteiro"))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Login = Ui_Login()
    Login.setupUi(Login)
    Login.exec_()
    sys.exit(app.exec_())